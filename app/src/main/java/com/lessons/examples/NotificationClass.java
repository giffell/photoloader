package com.lessons.examples;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.giffel.photoloader.MainActivity;
import com.android.giffel.photoloader.R;

public class NotificationClass extends Activity {

    private Button buttonNot;
    private Button buttonDialog;
    private Button buttonTimePickerDialog;
    private Button buttonDataPicker;
    private Button buttonAlertDialogTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);

        buttonDialog = (Button) findViewById(R.id.buttonDialog);
        buttonTimePickerDialog = (Button) findViewById(R.id.buttonTimePickerDialog);
        buttonNot=(Button)findViewById(R.id.buttonNot);
        buttonDataPicker = (Button) findViewById(R.id.buttonDataPicker);
        buttonAlertDialogTime = (Button) findViewById(R.id.buttonAlertDialogTime);



        buttonNot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Notify("New Message notify","You've received new message");
            }
        });
    }

    private void Notify(String notificationTitle, String notificationMessage){
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        @SuppressWarnings("deprecation")

        Notification notification = new Notification(R.drawable.ic_launcher,"New Message", System.currentTimeMillis());
        Intent notificationIntent = new Intent(this,NotificationClass.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,notificationIntent, 0);

        notification.setLatestEventInfo(NotificationClass.this, notificationTitle,notificationMessage, pendingIntent);
        notificationManager.notify(9999, notification);
    }

    public void buttonDialogListener(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v= inflater.inflate(R.layout.dialog_view, null);

        TextView textView= (TextView) v.findViewById(R.id.textViewDialog);
        ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.ic_launcher);
        textView.setText("It's custom view :)");
        builder.setMessage("Hello World")

                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                    }
                })
                .setNegativeButton("Don't okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        builder.setView(v);
        builder.create();
        builder.show();
    }



    public void buttonTimePickerDialogListener(View view) {
        Intent intent = new Intent(NotificationClass.this, TimePickerClass.class);
        startActivity(intent);
    }

    public void buttonDataPickerListener(View view) {
        Intent intent = new Intent(NotificationClass.this, DataPickerClass.class);
        startActivity(intent);

    }

    public void buttonAlertDialogTimeListener(View view) {
        Intent intent = new Intent(NotificationClass.this, AlertDialogTimeClass.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
