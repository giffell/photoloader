package com.lessons.examples;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.android.giffel.photoloader.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyListFragment extends ListFragment implements OnItemClickListener {

    private List<Integer> list;
    private CustomListProgressBar customListProgressBar;
    private Button buttonAddItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        buttonAddItem = (Button) view.findViewById(R.id.buttonAddItem);
        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customListProgressBar.add(new Random().nextInt());
            }
        });

        list = new ArrayList<>();
        list.add(5);
        list.add(10);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        customListProgressBar = new CustomListProgressBar(getActivity(), R.layout.listprogresscustom_view, list);
        customListProgressBar.setNotifyOnChange(true);
        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.Planets, android.R.layout.simple_list_item_1);

        setListAdapter(customListProgressBar);
        getListView().setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        Toast.makeText(getActivity(), "Item: " + position, Toast.LENGTH_SHORT)
                .show();

    }
}

