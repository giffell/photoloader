package com.lessons.examples;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import com.android.giffel.photoloader.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContainersClass extends Activity {

    private ExpandableListView expandableListView;

    String[] groups = new String[]{"HTC", "Samsung", "LG"};
    String[] phonesHTC = new String[]{"Sensation", "Desire", "Wildfire", "Hero"};
    String[] phonesSams = new String[]{"Galaxy S II", "Galaxy Nexus", "Wave"};
    String[] phonesLG = new String[]{"Optimus", "Optimus Link", "Optimus Black", "Optimus One"};
    ArrayList<Map<String, String>> groupData;
    ArrayList<Map<String, String>> childDataItem;
    ArrayList<ArrayList<Map<String, String>>> childData;
    Map<String, String> m;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contents_activity);

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);


        InitExpandableListVIew initExpandableListVIew = new InitExpandableListVIew().invoke();
        String[] groupFrom = initExpandableListVIew.getGroupFrom();
        int[] groupTo = initExpandableListVIew.getGroupTo();
        String[] childFrom = initExpandableListVIew.getChildFrom();
        int[] childTo = initExpandableListVIew.getChildTo();


        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this,
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                groupFrom,
                groupTo,
                childData,
                android.R.layout.simple_list_item_1,
                childFrom,
                childTo);
        expandableListView.setAdapter(adapter);

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class InitExpandableListVIew {
        private String[] groupFrom;
        private int[] groupTo;
        private String[] childFrom;
        private int[] childTo;

        public String[] getGroupFrom() {
            return groupFrom;
        }

        public int[] getGroupTo() {
            return groupTo;
        }

        public String[] getChildFrom() {
            return childFrom;
        }

        public int[] getChildTo() {
            return childTo;
        }

        public InitExpandableListVIew invoke() {
            groupData = new ArrayList<Map<String, String>>();
            for (String group : groups) {
                m = new HashMap<String, String>();
                m.put("groupName", group);
                groupData.add(m);
            }
            groupFrom = new String[]{"groupName"};
            groupTo = new int[]{android.R.id.text1};
            childData = new ArrayList<ArrayList<Map<String, String>>>();
            childDataItem = new ArrayList<Map<String, String>>();

            for (String phone : phonesHTC) {
                m = new HashMap<String, String>();
                m.put("phoneName", phone);
                childDataItem.add(m);
            }
            childData.add(childDataItem);

            childDataItem = new ArrayList<Map<String, String>>();
            for (String phone : phonesSams) {
                m = new HashMap<String, String>();
                m.put("phoneName", phone);
                childDataItem.add(m);
            }
            childData.add(childDataItem);

            childDataItem = new ArrayList<Map<String, String>>();
            for (String phone : phonesLG) {
                m = new HashMap<String, String>();
                m.put("phoneName", phone);
                childDataItem.add(m);
            }
            childData.add(childDataItem);

            childFrom = new String[]{"phoneName"};
            childTo = new int[]{android.R.id.text1};
            return this;
        }
    }
}
