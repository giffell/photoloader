package com.lessons.examples;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class SimpleService extends Service{

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v("MyLogs", "Start Service");
        startMessages();
        return super.onStartCommand(intent, flags, startId);
    }

    private void startMessages() {
                for (int i = 0; i < 200; i++) {
                    Log.v("MyLogs", "Number = " + i);
                }
        stopSelf();
    }

    @Override
    public void onDestroy() {
        Log.v("MyLogs", "Stop Service");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v("MyLogs", "onBind Service");
        return null;
    }
}
