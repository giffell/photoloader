package com.lessons.examples;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;

import com.android.giffel.photoloader.R;

import util.RequestCode;

public class VideoViewClass extends Activity {

    private VideoView videoView;
    private Button buttonVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videoview_activity);
        videoView = (VideoView) findViewById(R.id.videoView);
        buttonVideoView = (Button) findViewById(R.id.buttonVideoView);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void buttonOpenViewListener(View view) {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_OPEN_VIDEO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCode.REQUEST_CODE_OPEN_VIDEO:
                    Log.v("MyLogs", "VideoUploaded");

                    if (data.getData() != null) {
                        videoView.setVideoURI(data.getData());
                        videoView.start();
                        Log.v("MyLogs", "Video Start");
                    }
                    break;
            }

        }


        super.onActivityResult(requestCode, resultCode, data);
    }
}
