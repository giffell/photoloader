package com.lessons.examples;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.giffel.photoloader.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    private Button buttonAddItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = (ListView) findViewById(R.id.downloadListView);

        final List<DownloadInfo> downloadInfo = new ArrayList<DownloadInfo>();

        buttonAddItem = (Button) findViewById(R.id.buttonAddItem);
        buttonAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadInfo.add(new DownloadInfo("Progress "));
                listView.setAdapter(new DownloadInfoArrayAdapter(getApplicationContext(), R.id.downloadListView, downloadInfo));
            }
        });





//        for(int i = 0; i < 50; ++i) {
//            downloadInfo.add(new DownloadInfo("File " + i, 1000));
//        }
        //listView.setAdapter(new DownloadInfoArrayAdapter(getApplicationContext(), R.id.downloadListView, downloadInfo));
    }

}
