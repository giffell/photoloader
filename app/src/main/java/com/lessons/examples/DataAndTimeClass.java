package com.lessons.examples;

import android.app.Activity;
import android.os.Bundle;
import android.widget.AnalogClock;

import com.android.giffel.photoloader.R;

public class DataAndTimeClass extends Activity{

    private AnalogClock analogClock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_time_activity);
        analogClock = (AnalogClock) findViewById(R.id.analogClock);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
