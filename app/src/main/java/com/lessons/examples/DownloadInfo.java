package com.lessons.examples;

import android.util.Log;


public class DownloadInfo {
    private final static String TAG = DownloadInfo.class.getSimpleName();
    public enum DownloadState {
        NOT_STARTED,
        QUEUED,
        DOWNLOADING,
        COMPLETE
    }
    private volatile DownloadState mDownloadState = DownloadState.NOT_STARTED;
    private final String mFilename;
    private volatile Integer mProgress;
    private volatile CustomView mProgressBar;

    public DownloadInfo(String filename) {
        mFilename = filename;
        mProgress = 0;
        mProgressBar = null;
    }

    public CustomView getProgressBar() {
        return mProgressBar;
    }
    public void setProgressBar(CustomView progressBar) {
        Log.d(TAG, "setProgressBar " + mFilename + " to " + progressBar);
        mProgressBar = progressBar;
    }

    public void setDownloadState(DownloadState state) {
        mDownloadState = state;
    }
    public DownloadState getDownloadState() {
        return mDownloadState;
    }

    public Integer getProgress() {
        return mProgress;
    }

    public void setProgress(Integer progress) {
        this.mProgress = progress;
    }

    public String getFilename() {
        return mFilename;
    }
}
