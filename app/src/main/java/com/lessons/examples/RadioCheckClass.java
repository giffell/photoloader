package com.lessons.examples;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.giffel.photoloader.R;

public class RadioCheckClass extends Activity implements AdapterView.OnItemSelectedListener {

    private RadioButton buttonRadio1;
    private RadioButton buttonRadio2;
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private ToggleButton aSwitch;
    private Button buttoncheckSwitch;
    private ImageButton buttonImage;
    private SeekBar seekBar;
    private Button buttonSeek;
    private RatingBar ratingBar;
    private Spinner spinner;
    private WebView webView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radiocheck_activity);
        buttonRadio1 = (RadioButton) findViewById(R.id.radioButton);
        buttonRadio2 = (RadioButton) findViewById(R.id.radioButton2);
        checkBox1 = (CheckBox) findViewById(R.id.checkBox1);
        checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
        aSwitch = (ToggleButton) findViewById(R.id.switch1);
        buttoncheckSwitch = (Button) findViewById(R.id.buttoncheckSwitch);
        buttonImage = (ImageButton) findViewById(R.id.imageButton);
        buttonImage.setImageResource(R.drawable.ic_launcher);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setMax(50);
        seekBar.setProgress(0);
        buttonSeek = (Button) findViewById(R.id.buttonSeek);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(getApplicationContext(), String.valueOf(ratingBar.getProgress()),Toast.LENGTH_SHORT).show();
            }
        });
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.Planets, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        webView = (WebView) findViewById(R.id.webView);
        webView.loadUrl("http://www.example.com");
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void buttonClickListener(View view) {
        if (buttonRadio1.isChecked() && buttonRadio2.isChecked()) {
            Toast.makeText(this, "One and Two", Toast.LENGTH_SHORT).show();
            buttonRadio1.setChecked(false);
            buttonRadio2.setChecked(false);
        }
        else {
            if (buttonRadio1.isChecked()) {
                Toast.makeText(this, "One", Toast.LENGTH_SHORT).show();
                buttonRadio1.setChecked(false);
            }
            if (buttonRadio2.isChecked()) {
                Toast.makeText(this, "Two", Toast.LENGTH_SHORT).show();
                buttonRadio2.setChecked(false);
            }
        }
    }

    public void buttonClick2Listener(View view) {
        if (checkBox1.isChecked() && checkBox2.isChecked()) {
            Toast.makeText(this, "One and Two", Toast.LENGTH_SHORT).show();
            checkBox1.setChecked(false);
            checkBox2.setChecked(false);
        }
        else {
            if (checkBox1.isChecked()) {
                Toast.makeText(this, "One", Toast.LENGTH_SHORT).show();
                checkBox1.setChecked(false);
            }
            if (checkBox2.isChecked()) {
                Toast.makeText(this, "Two", Toast.LENGTH_SHORT).show();
                checkBox2.setChecked(false);
            }
        }
    }

    public void buttonCheckSwitchListener(View view) {
        if (aSwitch.isChecked()) Toast.makeText(this, "On", Toast.LENGTH_SHORT).show();
        else Toast.makeText(this, "Off", Toast.LENGTH_SHORT).show();
    }

    public void buttonImageListener(View view) {
        Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
    }
    public void buttonSeekListener(View view) {
        Toast.makeText(this, String.valueOf(seekBar.getProgress()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getApplicationContext(), String.valueOf(parent.getItemAtPosition(position)), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
