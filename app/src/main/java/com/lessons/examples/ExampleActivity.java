package com.lessons.examples;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.giffel.photoloader.R;

public class ExampleActivity extends Activity {

    private Button buttonPreference;
    private Button buttonSQlite;
    private Button buttonTouch;
    private Button buttonNotification;
    private Button buttonHandler;
    private Button buttonService;
    private Button buttonRadioButton;
    private Button buttonVideoView;
    private Button buttonDataAndTime;
    private Button buttonOther;
    private Button buttonAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.example_activity);
        buttonPreference = (Button) findViewById(R.id.buttonPreferences);
        buttonSQlite = (Button) findViewById(R.id.buttonSQLite);
        buttonTouch = (Button) findViewById(R.id.buttonTouch);
        buttonNotification = (Button) findViewById(R.id.buttonNotification);
        buttonHandler = (Button) findViewById(R.id.buttonHandler);
        buttonService = (Button) findViewById(R.id.buttonService);
        buttonRadioButton = (Button) findViewById(R.id.buttonRadioButton);
        buttonVideoView = (Button) findViewById(R.id.buttonVideoView);
        buttonDataAndTime = (Button) findViewById(R.id.buttonDataAndTime);
        buttonOther = (Button) findViewById(R.id.buttonOther);
        buttonAnimation = (Button) findViewById(R.id.buttonAnimation);

    }
    public void buttonPreferenceListener(View view) {
        Intent intent = new Intent(this, Preference.class);
        startActivity(intent);
    }

    public void buttonSQLiteListener(View view) {
        Intent intent = new Intent(this, SQLite.class);
        startActivity(intent);
    }

    public void buttonTouchListener(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void buttonNotification(View view) {
        Intent intent = new Intent(ExampleActivity.this, NotificationClass.class);
        startActivity(intent);
    }

    public void buttonHandlerListener(View view) {
        Intent intent = new Intent(ExampleActivity.this, HandlerClass.class);
        startActivity(intent);
    }

    public void buttonCustumView(View view) {
        Intent intent = new Intent(ExampleActivity.this, CustomView.class);
        startActivity(intent);
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public void buttonServiceListener(View view) {
        Intent intent = new Intent(ExampleActivity.this, ServiceClass.class);
        startActivity(intent);
    }

    public void buttonRadioListenetr(View view) {
        Intent intent = new Intent(ExampleActivity.this, RadioCheckClass.class);
        startActivity(intent);
    }

    public void buttonContainersListenetr(View view) {
        Intent intent = new Intent(ExampleActivity.this, ContainersClass.class);
        startActivity(intent);
    }

    public void buttonVideoViewListener(View view) {
        Intent intent = new Intent(ExampleActivity.this, VideoViewClass.class);
        startActivity(intent);
    }

    public void buttonDataAndTimeListener(View view) {
        Intent intent = new Intent(ExampleActivity.this, DataAndTimeClass.class);
        startActivity(intent);
    }

    public void buttonOtherListener(View view) {
        Intent intent = new Intent(ExampleActivity.this, OtherItemsClass.class);
        startActivity(intent);
    }

    public void buttonAnimationListener(View view) {
        Intent intent = new Intent(ExampleActivity.this, AnimationsClass.class);
        startActivity(intent);
    }
}
