package com.lessons.examples;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.giffel.photoloader.R;

import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

public class AnimationsClass extends Activity{

    private Button buttonDrawableAnimation;
    private Button buttonViewAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation_activity);
        buttonDrawableAnimation = (Button) findViewById(R.id.buttonDrawableAnimation);
        buttonViewAnimation = (Button) findViewById(R.id.buttonViewAnimation);
        animate(buttonDrawableAnimation).setDuration(2500).rotationYBy(1440).x(0).y(0);
        animate(buttonViewAnimation).setDuration(2500).rotationYBy(1440).x(200).y(500);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void buttonDrawableAnimationListener(View view) {
        Intent intent = new Intent(this, DrawableAnimation.class);
        startActivity(intent);
    }
}
