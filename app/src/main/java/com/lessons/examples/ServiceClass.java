package com.lessons.examples;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.giffel.photoloader.R;

public class ServiceClass extends Activity{

    private Button buttonStartService;
    private Button buttonStopService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_activity);
        buttonStartService = (Button) findViewById(R.id.buttonStartService);
        buttonStopService = (Button) findViewById(R.id.buttonStopService);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void buttonStartServiceListener(View view) {
        startService(new Intent(ServiceClass.this, SimpleService.class));
    }

    public void buttonStopServiceListener(View view) {
        stopService(new Intent(ServiceClass.this, SimpleService.class));
    }
}
