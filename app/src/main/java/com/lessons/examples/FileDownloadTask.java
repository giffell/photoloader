package com.lessons.examples;

import android.os.AsyncTask;
import android.util.Log;

/**
 * Simulate downloading a file, by increasing the progress of the FileInfo from 0 to size - 1.
 */
public class FileDownloadTask extends AsyncTask<Void, Integer, Void> {
    private static final String TAG = FileDownloadTask.class.getSimpleName();
    final DownloadInfo mInfo;

    public FileDownloadTask(DownloadInfo info) {
        mInfo = info;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        mInfo.setProgress(values[0]);
        CustomView bar = mInfo.getProgressBar();
        if (bar != null) {
            bar.setProgress(mInfo.getProgress());
            bar.invalidate();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        Log.d(TAG, "Starting download for " + mInfo.getFilename());
        mInfo.setDownloadState(DownloadInfo.DownloadState.DOWNLOADING);
        for (int i = 0; i <= 1000; ++i) {
            try {
                Thread.sleep(16);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishProgress(i);
        }
        mInfo.setDownloadState(DownloadInfo.DownloadState.COMPLETE);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        mInfo.setDownloadState(DownloadInfo.DownloadState.COMPLETE);
    }

    @Override
    protected void onPreExecute() {
        mInfo.setDownloadState(DownloadInfo.DownloadState.DOWNLOADING);
    }

}
