package com.lessons.examples;

import android.app.Activity;
import android.os.Bundle;

import com.android.giffel.photoloader.R;

public class OtherItemsClass extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.other_items);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
