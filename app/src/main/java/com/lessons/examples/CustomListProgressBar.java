package com.lessons.examples;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.giffel.photoloader.R;

import java.util.List;

public class CustomListProgressBar extends ArrayAdapter<Integer> {

    private Context c;
    private int id;
    private List<Integer> items;

    public CustomListProgressBar(Context context, int resource, List<Integer> items) {
        super(context, resource, items);
        c = context;
        id = resource;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;

        if (row == null) {
            LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = vi.inflate(id, null);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        }
        else {
            holder = (MyViewHolder) row.getTag();
        }

        int val = items.get(position);

            if (holder.getProgressbar() != null) {

                holder.getProgressbar().setProgress(val);
            }
            if (holder.getTextpercents() != null) {
                holder.getTextpercents().setText(String.valueOf(val));
            }
        if (holder.getTextprogress() != null) {
            holder.getTextprogress().setText("Progress");
        }
        return row;
    }



    private class MyViewHolder {
        CustomView progressbar;
        TextView textprogress;
        TextView textpercents;

        public MyViewHolder(View view) {
            progressbar = (CustomView) view.findViewById(R.id.customProgress);
            textprogress = (TextView) view.findViewById(R.id.textProgress);
            textpercents = (TextView) view.findViewById(R.id.percents);
        }

        public CustomView getProgressbar() {
            return progressbar;
        }

        public void setProgressbar(CustomView progressbar) {
            this.progressbar = progressbar;
        }

        public TextView getTextprogress() {
            return textprogress;
        }

        public void setTextprogress(TextView textprogress) {
            this.textprogress = textprogress;
        }

        public TextView getTextpercents() {
            return textpercents;
        }

        public void setTextpercents(TextView textpercents) {
            this.textpercents = textpercents;
        }
    }
}
