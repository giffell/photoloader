package com.lessons.examples;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.android.giffel.photoloader.R;

public class HandlerClass extends Activity {

    private Handler handler;
    private CustomView progressBar;
    private Button buttonStart;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.handler_activity);
        handler = new Handler();
        progressBar = (CustomView) findViewById(R.id.progressBar1);
        buttonStart = (Button) findViewById(R.id.buttonStart);

    }


    public void startProgress(View view) {
            buttonStart.setEnabled(false);
            new Thread(new Task()).start();
    }


    class Task implements Runnable {
        @Override
        public void run() {
            for (int i = 0; i <= 100; i++) {
                final int value = i;
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(value);
                    }
                });
            }
            HandlerClass.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    buttonStart.setEnabled(true);
                }
            });
        }

    }


}
