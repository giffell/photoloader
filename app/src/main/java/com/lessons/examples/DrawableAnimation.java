package com.lessons.examples;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.android.giffel.photoloader.R;

public class DrawableAnimation extends Activity{

    private Button buttonStartStop;
    private ImageView imageViewDrawable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawable_animation);
        buttonStartStop = (Button) findViewById(R.id.buttonStartStop);
        imageViewDrawable = (ImageView) findViewById(R.id.imageViewDrawable);
        imageViewDrawable.setBackgroundResource(R.drawable.animball);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void buttonStartStopListener(View view) {
        AnimationDrawable animationDrawable = (AnimationDrawable) imageViewDrawable.getBackground();
        if (animationDrawable.isRunning()) {
            buttonStartStop.setText("Start");
            animationDrawable.stop();
        }
        else {
            buttonStartStop.setText("Stop");
            animationDrawable.start();
        }
    }
}
