package com.filechooser;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.giffel.photoloader.R;
import com.nhaarman.listviewanimations.appearance.simple.AlphaInAnimationAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class FileChooser extends ListActivity {

    private File currentDir;
    private FileArrayAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.file_view);
        currentDir = new File("/");
        //fillMainDirs(currentDir);
        fill(currentDir);
    }

//    private void fillMainDirs(File f) {
//        File[] dirs = f.listFiles();
//        this.setTitle("Main: " + f.getName());
//        List<Option> dir = new ArrayList<Option>();
//        List<Option> fls = new ArrayList<Option>();
//
//        CharSequence charSequence = "sdcard";
//        try {
//            for (File ff : dirs) {
//                if (ff.isDirectory() && ff.getName().contains(charSequence)) {
//                    dir.add(new Option(ff.getName(), "Folder", ff.getAbsolutePath()));
//                }
//
//            }
//            // final String path = android.os.Environment.DIRECTORY_DCIM;
//            //File pathFile = new File(path);
//            //dir.add(new Option("Gallery", "Folder", android.os.Environment.DIRECTORY_DCIM));
//
//        } catch (Exception e) {
//
//        }
//        Collections.sort(dir);
//        Collections.sort(fls);
//        dir.addAll(fls);
//        if (!f.getName().equalsIgnoreCase("sdcard"))
//            dir.add(0, new Option("..", "Parent Directory", f.getParent()));
//        adapter = new FileArrayAdapter(FileChooser.this, R.layout.file_view, dir);
//        this.setListAdapter(adapter);
//    }

    private void fill(File f) {
        File[] dirs = f.listFiles();
        this.setTitle("Current Dir: " + f.getName());
        List<Option> dir = new ArrayList<Option>();
        List<Option> fls = new ArrayList<Option>();
        try {
            for (File ff : dirs) {
                if (ff.isDirectory())
                    dir.add(new Option(ff.getName(), "Folder", ff.getAbsolutePath()));
                else {
                    fls.add(new Option(ff.getName(), "File Size: " + ff.length(), ff.getAbsolutePath()));
                }
            }
        } catch (Exception e) {

        }
        Collections.sort(dir);
        Collections.sort(fls);
        dir.addAll(fls);
        if (!f.getName().equalsIgnoreCase("sdcard"))
            dir.add(0, new Option("..", "Parent Directory", f.getParent()));
        adapter = new FileArrayAdapter(FileChooser.this, R.layout.file_view, dir);
        AlphaInAnimationAdapter animationAdapter = new AlphaInAnimationAdapter(adapter);
        animationAdapter.setAbsListView(this.getListView());
//        this.getListView().setAdapter(animationAdapter);
        this.setListAdapter(animationAdapter);
    }

    Stack<File> dirStack = new Stack<File>();

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Option o = adapter.getItem(position);
        if (o.getData().equalsIgnoreCase("folder")) {
            dirStack.push(currentDir);
            currentDir = new File(o.getPath());
            fill(currentDir);
        } else if (o.getData().equalsIgnoreCase("parent directory")) {
            if (dirStack.size() == 0) {
                Log.v("DIR", "dirStack = 0");
                finish();
            }
            else {
                currentDir = dirStack.pop();
                fill(currentDir);
            }
        } else {
            onFileClick(o);



        }

    }

    private void onFileClick(Option o) {
        Toast.makeText(this, "File Clicked: " + o.getName(), Toast.LENGTH_SHORT).show();

        this.getIntent().putExtra("path", o.getPath());
        setResult(RESULT_OK, getIntent());
        Log.v("DIR ", o.getPath());
        Log.v("DIR ", "Finish");
        finish();

    }



    @Override
    public void onBackPressed() {
        if (dirStack.size() == 0) {
            Log.v("DIR", "dirStack = 0");
            return;
        }
        //fillMainDirs(currentDir);

        if (dirStack.size() != 1) {
            Log.v("DIR", currentDir.getName());
            Log.v("DIR", String.valueOf(dirStack.size()));
            currentDir = dirStack.pop();
            Log.v("DIR", currentDir.getName());
            Log.v("DIR", String.valueOf(dirStack.size()));
            fill(currentDir);
        }
        else {
            Log.v("DIR", "fillMainDirs");
            //currentDir = new File("/storage/");
            currentDir = dirStack.pop();
            fill(currentDir);
            //fillMainDirs(currentDir);
        }
//
    }
}
