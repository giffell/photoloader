package com.filechooser;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.giffel.photoloader.R;

import java.util.List;


public class FileArrayAdapter extends ArrayAdapter<Option> {

    private static final String TAG = "myLog";
    private Context c;
    private int id;
    private List<Option> items;

    public FileArrayAdapter(Context context, int textViewResourceId,
                            List<Option> objects) {
        super(context, textViewResourceId, objects);
        c = context;
        id = textViewResourceId;
        items = objects;
    }

    @Override
    public Option getItem(int i) {
        return items.get(i);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder = null;

        if (row == null) {
            LayoutInflater vi = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = vi.inflate(id, null);
            holder = new MyViewHolder(row);
            row.setTag(holder);
            Log.v(TAG, " Creating a new row");
        }
        else {
            holder = (MyViewHolder) row.getTag();
            Log.v(TAG, " Recycling stuff");
        }

        final Option o = items.get(position);
        if (o != null) {

            if (holder.getT1() != null)
                holder.getT1().setText(o.getName());
            if (holder.getT2() != null)
                holder.getT2().setText(o.getData());
            if (holder.getImg1() != null)
                holder.getImg1().setImageResource(R.drawable.ic_launcher);
        }
        return row;
    }

    private class MyViewHolder {
        TextView t1;
        TextView t2;
        ImageView img1;

        public MyViewHolder(View view) {
            t1 = (TextView) view.findViewById(R.id.TextView01);
            t2 = (TextView) view.findViewById(R.id.TextView02);
            img1 = (ImageView) view.findViewById(R.id.imageViewIcon);
        }

        public TextView getT1() {
            return t1;
        }

        public void setT1(TextView t1) {
            this.t1 = t1;
        }

        public TextView getT2() {
            return t2;
        }

        public void setT2(TextView t2) {
            this.t2 = t2;
        }

        public ImageView getImg1() {
            return img1;
        }

        public void setImg1(ImageView img1) {
            this.img1 = img1;
        }
    }

}
