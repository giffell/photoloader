package com.android.giffel.photoloader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.giffel.photoloader.fragments.Home;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.apache.commons.lang3.ArrayUtils;

import util.UtilMetods;

/**
 * Created by Alexander on 29.06.2015.
 */
public class GridViewImage extends Activity {

    final String LOG_TAG = "myLogs";
    private String[] path;
    private int[] id_image;
    private GridView gridview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gridview_activity);
        path = getIntent().getStringArrayExtra("data");
        id_image = getIntent().getIntArrayExtra("id");
        for (int i = 0; i < id_image.length; i++) {
            Log.v("myLog", "id = " + id_image[i]);
            Log.v("myLog", "path =  = " + path[i]);
            Log.v("myLog", "-----------------------------------------------------------------");

        }
        gridview = (GridView) findViewById(R.id.gridview);

        //SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(new ImageAdapter(this));
        //swingBottomInAnimationAdapter.setAbsListView(gridview);
        //assert swingBottomInAnimationAdapter.getViewAnimator() != null;
        //swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(300);
        //gridview.setAdapter(swingBottomInAnimationAdapter);
        ImageAdapter imageAdapter = new ImageAdapter(GridViewImage.this);
        imageAdapter.notifyDataSetChanged();
        gridview.setAdapter(imageAdapter);
        gridview.setNumColumns(GridView.AUTO_FIT);
        gridview.setColumnWidth(200);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                //Toast.makeText(GridViewImage.this, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(GridViewImage.this, Home.class);
                intent.putExtra("data", path[position]);
                Log.v(LOG_TAG, "data = " + path[position]);
                UtilMetods.simpleOpen = true;
                startActivity(intent);
                Log.v(LOG_TAG, "FINISH = ");
                finish();

            }
        });

        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.v(LOG_TAG, "Long click position = " + position);
                SQLiteImageDB sqLiteImageDB = new SQLiteImageDB(GridViewImage.this);
                sqLiteImageDB.deleteById(id_image[position]);
                id_image = ArrayUtils.remove(id_image, position);
                path = ArrayUtils.remove(path, position);
                //removeIntElement(id_image, position);
                //removeStringElement(path, position);
                ImageAdapter imageAdapter = new ImageAdapter(GridViewImage.this);
                imageAdapter.notifyDataSetChanged();
                gridview.setAdapter(imageAdapter);
                Toast.makeText(GridViewImage.this, "Image was deleted", Toast.LENGTH_SHORT).show();
//                for (int i = 0; i < id_image.length; i++) {
//                    Log.v("myLog", "id = " + id_image[i]);
//                }
                //gridview.invalidateViews();
                return true;
            }
        });
//        gridview.setAdapter(new ImageAdapter(this));
//        gridview.invalidateViews();

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
            initImageLoader(mContext);
        }

        public void initImageLoader(Context context) {
            // This configuration tuning is custom. You can tune every option, you may tune some of them,
            // or you can create default configuration by
            //  ImageLoaderConfiguration.createDefault(this);
            // method.
            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
            config.discCacheFileNameGenerator(new Md5FileNameGenerator());
            config.discCacheSize(50 * 1024 * 1024);
            config.tasksProcessingOrder(QueueProcessingType.FIFO);
            config.writeDebugLogs(); // Remove for release app

            // Initialize ImageLoader with configuration.
            ImageLoader.getInstance().init(config.build());
        }

        @Override
        public int getCount() {
            return path.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ImageView imageView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 4, 4, 4);
            } else {
                imageView = (ImageView) convertView;
            }
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageSize imageSize = new ImageSize(200, 200);
            BitmapFactory.Options bitopOptions = new BitmapFactory.Options();
            bitopOptions.outWidth = 200;
            bitopOptions.outHeight = 200;

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .resetViewBeforeLoading(false)  // default
                    .delayBeforeLoading(100)
                    .showImageOnLoading(R.drawable.start)
                    .considerExifParams(false) // default
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                    .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                    .cacheOnDisc(true)
                    .decodingOptions(bitopOptions)
                    .build();
            if (path[position].contains("MyCameraApp")) {
                imageLoader.loadImage(path[position], options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        Log.v("myLog", "Start Load Image");
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        Log.v("myLog", "Failed Load Image " + imageUri + " " + failReason.getCause().toString());
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, final Bitmap loadedImage) {
                        Log.v("myLog", "Complete Load Image " + imageUri);
                        GridViewImage.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(loadedImage);
                            }
                        });

                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        Log.v("myLog", "Cancel Load Image " + imageUri);
                    }
                });

            } else imageLoader.displayImage(path[position], imageView, options);

//            String realPath;
//            // SDK < API11
//            try {
//                if (Build.VERSION.SDK_INT < 11)
//                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(GridViewImage.this, Uri.parse(path[position]));
//
//                    // SDK >= 11 && SDK < 19
//                else if (Build.VERSION.SDK_INT < 19)
//                    realPath = RealPathUtil.getRealPathFromURI_API11to18(GridViewImage.this, Uri.parse(path[position]));
//
//                    // SDK > 19 (Android 4.4)
//                else
//                    realPath = RealPathUtil.getRealPathFromURI_API19(GridViewImage.this, Uri.parse(path[position]));
//
//
//
//
////                String pathFile = "";
////                if (path[position].toLowerCase().startsWith("file://")) {
////                    // Selected file/directory path is below
////                    pathFile = (new File(URI.create(pathFile))).getAbsolutePath();
////                    imageView.setImageBitmap(BitmapFactory.decodeFile(pathFile));
////                    return imageView;
////                    //TODO NEED TO DECODE IMAGEm Bitmap very Large
////                }
//
//
//                //TODO Problems with Some images. Image works fine from Gallery. May be use Uri.parse(path[position]
//                Uri imgUri = Uri.parse(realPath);

//
//                //imageView.setImageURI(null);
//                //imageView.setImageURI(imgUri);
//
//            } catch (Exception e) {
//                Log.v("MyLog", "Catch");
//                Uri imgUri = Uri.parse(path[position]);
//                imageView.setImageURI(null);
//                Log.v("MyLog", imgUri + "Catch");
//                imageView.setImageURI(imgUri);
//            }

            return imageView;
        }
        //TODO some problem with camera's photo
        // references to our images
        //private Integer[] mThumbIds;
    }


}
