package com.android.giffel.photoloader;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import java.util.HashMap;

public class ContentDBProvider extends ContentProvider {

    public static final String AUTHORITY = "com.android.giffel.photoloader.provider";
    public static final String PATH = "imagetable";

    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + PATH);

    // fields for the database
    public static final String ID = "id";
    public static final String NAME = "name";

    static final int IMAGES = 1;
    static final int IMAGES_ID = 2;

    // maps content URI "patterns" to the integer values that were set above
    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, PATH, IMAGES);
        uriMatcher.addURI(AUTHORITY, PATH + "/#", IMAGES_ID);
    }

    private SQLiteDatabase database;
    private static HashMap<String, String> BirthMap;
    private DBHelper dbHelper;


    @Override
    public boolean onCreate() {
        Context context = getContext();
        dbHelper = new DBHelper(context);
        // permissions to be writable
        database = dbHelper.getWritableDatabase();
        return database != null;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on
        queryBuilder.setTables(PATH);

        switch (uriMatcher.match(uri)) {
            // maps all database column names
            case IMAGES:
                queryBuilder.setProjectionMap(BirthMap);
                break;
            case IMAGES_ID:
                queryBuilder.appendWhere(ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == "") {
            // No sorting-> sort on names by default
            sortOrder = NAME;
        }
        Cursor cursor = queryBuilder.query(database, projection, selection,
                selectionArgs, null, null, sortOrder);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;

    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            // Get all friend-birthday records
            case IMAGES:
                return "vnd.android.cursor.dir/vnd.example." + PATH;
            // Get a particular friend
            case IMAGES_ID:
                return "vnd.android.cursor.item/vnd.example." + PATH;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int count = 0;

        switch (uriMatcher.match(uri)) {
            case IMAGES:
                // delete all the records of the table

                database = dbHelper.getWritableDatabase();
                int cnt = database.delete(PATH, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);

//                count = database.delete(PATH, "id = " + 0, null);
//                Log.v("myLog", String.valueOf(IMAGES));
                break;
            case IMAGES_ID:

                database = dbHelper.getWritableDatabase();
                int cnta = database.delete(PATH, selection, selectionArgs);
                getContext().getContentResolver().notifyChange(uri, null);

//                Log.v("myLog", String.valueOf(IMAGES_ID));
//                count = database.delete(PATH, "id = " + 0, null);
//                String id = uri.getLastPathSegment(); //gets the id
//                count = database.delete(PATH, ID + " = " + id +
//                        (!TextUtils.isEmpty(selection) ? " AND (" +
//                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "imageDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("LOG_TAG", "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table imagetable ("
                    + "id integer primary key autoincrement,"
                    + "name text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
