package com.android.giffel.photoloader.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.giffel.photoloader.R;

public class NotificationService extends Service {

    NotificationManager notificationManager;
    private SharedPreferences sPref;

    @Override
    public void onCreate() {
        super.onCreate();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Log.v("myLog", "Service onCreate");

    }

    private void startNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        //icon appears in device notification bar and right hand corner of notification
        builder.setSmallIcon(R.mipmap.ic_upload_white_18dp);

        // This intent is fired when notification is clicked
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com/"));
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Set the intent that will fire when the user taps the notification.
        builder.setContentIntent(pendingIntent);

        // Large icon appears on the left of the notification
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_upload_white_48dp));

        // Content title, which appears in large type at the top of the notification
        builder.setContentTitle("Image uploaded link");

        // Content text, which appears in smaller text below the title
        builder.setContentText("MyUrl");



        // The subtext, which appears under the text on newer devices.
        // This will show-up in the devices with Android 4.2 and above only
        //builder.setSubText("Tap to view documentation about notifications.");



        // Will display the notification in the notification bar
        notificationManager.notify(1, builder.build());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startNotification();
        Log.v("myLog", "Service onStartCommand");
        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("flag", "1");
        ed.commit();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        String savedText = sPref.getString("flag", "");
        Log.v("myLog", savedText);
        Log.v("myLog", "Service onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
