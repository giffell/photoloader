package com.android.giffel.photoloader.Service;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.giffel.photoloader.CustomMultiPartEntity;
import com.android.giffel.photoloader.R;
import com.android.giffel.photoloader.ResultProcessing;
import com.android.giffel.photoloader.fragments.Home;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class UploadService extends Service {

    private PendingIntent pi;
    private PendingIntent pii;
    private long totalLength;
    private HttpResponse response;
    private String responseStr;
    private int percentUploaded;
    private String finalUrl;
    private AsyncClass asyncClass;
    private SharedPreferences sPref;
    private Notification note;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
        Log.v("myLog", "Destroy UploadService");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pi = intent.getParcelableExtra(Home.PARAM_PINTENT);

        //someWork();
        Log.v("myLog", "StartService");
        note = new Notification(R.mipmap.ic_upload_white_48dp,
                "New Message",
                System.currentTimeMillis());
        Intent i = new Intent(this, Home.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);

        pii = PendingIntent.getActivity(this, 0,
                i, 0);
        note.setLatestEventInfo(this, "Uploading...Please wait", "", pii);
        //note.flags|= Notification.FLAG_NO_CLEAR;

        asyncClass = new AsyncClass();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncClass.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
        } else {
            asyncClass.execute((Void) null);
        }
        startForeground(1337, note);
//        return super.onStartCommand(intent, flags, startId);
        return Service.START_REDELIVER_INTENT;
    }

    private void someWork() {
        try {

            pi.send(Home.STATUS_START);
            TimeUnit.SECONDS.sleep(3);
            Intent intent = new Intent().putExtra(Home.PARAM_RESULT, "service my");
            pi.send(UploadService.this, Home.STATUS_FINISH, intent);

        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    class AsyncClass extends AsyncTask<Void, Integer, Void> {

        AlertDialog.Builder urlImage;
        Intent serviceIntent;
        long totalSize;

        @Override
        protected void onPreExecute() {
            try {
                pi.send(Home.STATUS_INIT);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }


            //notification = new NotificationCompat.Builder(getApplicationContext());


//            notification = new Notification(R.drawable.notification_template_icon_bg, "simulating a download", System
//                    .currentTimeMillis());
//            notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;
//            notification.contentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.download_progress);
//            notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.abc_ic_menu_paste_mtrl_am_alpha);
//            notification.contentView.setTextViewText(R.id.status_text, "simulation in progress");
//            notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);

//            notificationManager = (NotificationManager) getApplicationContext().getSystemService(
//                    getApplicationContext().NOTIFICATION_SERVICE);

//            notificationManager.notify(42, notification);


        }

        @Override
        protected void onPostExecute(Void aVoid) {


            Intent intent = new Intent(UploadService.this, Home.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(UploadService.this, 0, intent, 0);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(UploadService.this);
            builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.mipmap.ic_upload_white_48dp)
                    .setTicker("New Message")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Image successfully uploaded");

            Notification notification = builder.getNotification();
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(123, notification);

            stopForeground(true);

//            note.setLatestEventInfo(UploadService.this, "Image successfully uploaded", "", pii);
//            startForeground(1337, note);
//            try {
//                pi.send(Home.STATUS_ALERT);
//            } catch (PendingIntent.CanceledException e) {
//                e.printStackTrace();
//            }


//            final ResultProcessing resultProcessing = new ResultProcessing();
//
//            urlImage = createAlertUrlDialog(resultProcessing);
//
//            serviceIntent = new Intent();
//            serviceIntent.setAction("services.notificationService");
//            home.startService(serviceIntent);
//
//            urlImage.show();

//            urlImage.setCanceledOnTouchOutside(true);
//            urlImage.setContentView(R.layout.urlimage);
//            urlImage.show();
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    urlImage.cancel();
//                }
//            },5000);
            return;
        }

//        private AlertDialog.Builder createAlertUrlDialog(final ResultProcessing resultProcessing) {
//            return new AlertDialog.Builder(home)
//                    .setTitle("Image Url")
//                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            home.stopService(serviceIntent);
//                        }
//                    })
//                    .setPositiveButton("Open", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//
//                            String url = resultProcessing.getImageUrl(home.finalUrl);
//                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            i.setData(Uri.parse(url));
//                            home.stopService(serviceIntent);
//                            home.startActivity(i);
//                        }
//                    })
//                    .setMessage(resultProcessing.getImageUrl(home.finalUrl));
//        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //notification.contentView.setProgressBar(R.id.status_progress, 100, values[0], false);
            //notificationManager.notify(42, notification);
            try {
                Intent intent = new Intent().putExtra(Home.PARAM_UPDATE_VALUE, values[0]);
                pi.send(UploadService.this, Home.STATUS_PROGRESS_UPDATE, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            File file = new File(Environment.getExternalStorageDirectory() + "/tmp.jpg");


            final String absFilePath = file.getAbsolutePath();
            //HttpResponse response = null;


            String textFile = absFilePath;
            //Log.v(TAG + "TAG", "textFile: " + textFile);

            // the URL where the file will be posted
            String postReceiverUrl = "http://deviantsart.com/upload.php";
            //Log.v(TAG + "TAG", "postURL: " + postReceiverUrl);

            // new HttpClient
            HttpClient httpClient = new DefaultHttpClient();

            // post header
            HttpPost httpPost = new HttpPost(postReceiverUrl);

            file = new File(textFile);
            if (file.exists()) {
//                Log.v(TAG + "TAG", "File exists");
            }
            FileBody fileBody = new FileBody(file);
            totalLength = fileBody.getContentLength();
            CustomMultiPartEntity reqEntity = new CustomMultiPartEntity(
                    HttpMultipartMode.BROWSER_COMPATIBLE, new CustomMultiPartEntity.ProgressListener() {
                int lastPercent = 0;

                @Override
                public void transferred(long num) {

                    percentUploaded = (int) (((float) num) / ((float) totalLength) * 99f);
                    try {
                        TimeUnit.MILLISECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (num <= totalLength) {

                        percentUploaded = (int) (((float) num * 99f) / ((float) totalLength));
//                                pd.setProgress(percentUploaded);
                        Log.d("myLog", "percent uploaded: " + percentUploaded + " - " + num + " / " + totalLength);
                        //progress = home.percentUploaded;
                        publishProgress(percentUploaded);
//                                notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);
//                                notificationManager.notify(42, notification);
                        //lastPercent = percentUploaded;
                    }
                    if (percentUploaded >= 98) {
                        publishProgress(100);
                    }

                }

            });

            //MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("file", fileBody);
            httpPost.setEntity(reqEntity);

            // execute HTTP post request

            try {
                response = httpClient.execute(httpPost);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {


                try {
                    responseStr = EntityUtils.toString(resEntity).trim();
                    Log.v("myLog", "Response: " + responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            file.delete();
            //Log.v("myLog", home.imageOpenGlobalUri.getEncodedAuthority() + " " + home.imageOpenGlobalUri.getEncodedPath());

            /********************************************************************************/
            //addToDBLink(imageOpenGlobalUri.toString());
            /**********************************************************************************/

            //pd.cancel();
//            notificationManager.cancel(42);
            ResultProcessing resultProcessing = new ResultProcessing();
            finalUrl = responseStr;
            String res = resultProcessing.getImageUrl(finalUrl);
            try {
                Intent intent = new Intent().putExtra(Home.PARAM_RESULT, res);
                pi.send(UploadService.this, Home.STATUS_ALERT, intent);
                pi.send(UploadService.this, Home.STATUS_FINISH, intent);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
            Log.v("myLog", "RESULT SERVICE = " + res);

            sPref = PreferenceManager.getDefaultSharedPreferences(UploadService.this);
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString("flag", "1");
            ed.putString("resultUrl", res);
            ed.commit();
            //Log.v(TAG + " = ", resultProcessing.getImageUrl(home.finalUrl));
            // TODO FIX TO UPLOAD SMALL SIZE IMAGE, May be tmp. Origin image will download now.
            return null;
        }


    }

}
