package com.android.giffel.photoloader.fragments;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.giffel.photoloader.ParcelObject;
import com.android.giffel.photoloader.R;
import com.android.giffel.photoloader.SQLiteImageDB;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import util.RequestCode;
import util.UtilMetods;

public class Home extends ActionBarActivity {

    private static final String TAG = "myLog";
    public final int SERVICE_CODE = 1;
    public final static int STATUS_START = 299;
    public final static int STATUS_FINISH = 298;
    public final static int STATUS_IN_PROGRESS = 297;
    public static final int STATUS_INIT = 296;
    public static final int STATUS_ALERT = 295;
    public static final int STATUS_PROGRESS_UPDATE = 294;

    public final static String PARAM_PINTENT = "pendingIntent";
    public final static String PARAM_RESULT = "result";
    public final static String PARAM_UPDATE_VALUE = "value";


    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    private Drawer drawerBuilder;
    private Toolbar toolBar;
    private MenuItem mSearchAction;

    private ImageView imageViewArea;
    private Uri imageOpenGlobalUri;
    private Uri imageUri;
    private Bitmap bitmap;
    private Uri fileUri;
    private Uri uri3;
    private long totalLength;
    private HttpResponse response;
    private String responseStr;
    private int percentUploaded = 0;

    private final String MAIN_TAG = "MAIN";
    private final String SEARCH_TAG = "SEARCH";
    private final String HISTORY_TAG = "HISTORY";

    private MainFragment mainFragment;
    private String finalUrl;

    private Bundle savedInstance;
    //    private AsyncClass asyncClass;
    private ProgressDialog pd;

    private SharedPreferences sPref;

    public Intent servIntent;
    public PendingIntent pi;
    private AlertDialog.Builder urlImage;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    private ActionBar action;
    private InputMethodManager imm;
    private SearchFragment searchFragment;
    private HistoryFragment historyFragment;
    private GridView searchGridView;
    private GridView historyGridView;
    private RequestQueue queue;
    private List<String> list;
    private ImageAdapter imageAdapter;
    private ImageHistoryAdapter imageHistoryAdapter;
    private String[] items;
    private int[] id;
    private ProgressDialog progressDialog;
    private AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_fragment);

        savedInstance = savedInstanceState;

        Log.v(TAG, " onCreate");

        fragmentManager = getSupportFragmentManager();

        toolBar = (Toolbar) findViewById(R.id.toolbar1);

        if (toolBar != null) {
            setSupportActionBar(toolBar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setMainFragment();

//        if (savedInstance != null) asyncClass = (AsyncClass) getLastCustomNonConfigurationInstance();


        initialiseNavigationDrawer();
        //setContentView();
    }

    private void setMainFragment() {
        if (mainFragment == null) {
            mainFragment = new MainFragment();
        }
        Log.v(TAG, "hashcode = " + mainFragment.hashCode());
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.homeContainer, mainFragment, MAIN_TAG);
        transaction.commit();
        fragmentManager.executePendingTransactions();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        Log.v(TAG, " onResumeFragments");
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (imageOpenGlobalUri != null) {
            sPref = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor ed = sPref.edit();
            ed.putString("imageOpenGlobalUri", imageOpenGlobalUri.toString());
            ed.commit();
        }

//        sPref = PreferenceManager.getDefaultSharedPreferences(this);
//        String savedText = sPref.getString("TEXT", "");
//        Log.v("myLog", "Activity " + savedText);
        Log.v(TAG, " onStop");
    }


    @Override
    protected void onStart() {
        super.onStart();

        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        String savedText = sPref.getString("flag", "");
        if (savedText != null && savedText.equals("1")) {
            resetServiceFlag();
            sPref = PreferenceManager.getDefaultSharedPreferences(this);
            String imageglobal = sPref.getString("imageOpenGlobalUri", "");
            String resultUrl = sPref.getString("resultUrl", "");

            try {
                Intent service_intent = Intent.getIntent(sPref.getString("serviceIntent", ""));
                stopService(service_intent);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
//            String ns = Context.NOTIFICATION_SERVICE;
//            NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
//            mNotificationManager.cancel(1337);
            Log.v("myLog", "on Start Activity " + imageglobal);
            Log.v("myLog", "on Start Activity " + resultUrl);
            Uri urii = Uri.parse(imageglobal);
            decodeAndViewImage(urii);
            imageOpenGlobalUri = urii;
            urlImage = createAlertUrlDialog(resultUrl);
            urlImage.show();

        }
//        Log.v("myLog", "on Start Activity " + savedText);

        if (UtilMetods.simpleOpen) {
            if (getIntent().getStringExtra("data") != null) {
                Uri gridImageUri = Uri.parse(getIntent().getStringExtra("data"));
                Log.v(TAG, " data not null = " + gridImageUri);
                decodeAndViewImage(gridImageUri);
                imageOpenGlobalUri = gridImageUri;
                getIntent().setData(null);
            }
        }


        if (getIntent().getData() != null) {
            Log.v(TAG, " data OPEN = " + getIntent().getData().toString());
            decodeAndViewImage(getIntent().getData());
            imageOpenGlobalUri = Uri.parse(getIntent().getData().toString());
            getIntent().setData(null);
        }

        if (savedInstance != null) {
            Log.d("myLog", "returnBitmap");
            final ParcelObject parcelObject = (ParcelObject) savedInstance.getParcelable("KEY_BITMAP");
            imageOpenGlobalUri = Uri.parse(savedInstance.getString("KEY_ImageOpenGlobalUri"));
            bitmap = parcelObject.bitmap;
            imageViewArea = (ImageView) mainFragment.getView().findViewById(R.id.imageViewArea);
            Home.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    imageViewArea.setImageBitmap(parcelObject.bitmap);
                }
            });

        }

        Log.v(TAG, " onStart");
    }

    private void resetServiceFlag() {
        sPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString("flag", "0");
        ed.commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("myLog", "saveBitmap");
        ParcelObject parcelObject = new ParcelObject(bitmap);
        outState.putParcelable("KEY_BITMAP", parcelObject);
        outState.putString("KEY_ImageOpenGlobalUri", String.valueOf(imageOpenGlobalUri));
    }

    @Override
    protected void onDestroy() {
        Log.v(TAG, " onDestroy");
        super.onDestroy();
    }

    //    @Override
//    protected void onStart() {
//        super.onStart();
//
//
//    }

    @Override
    public void onBackPressed() {

        if (drawerBuilder != null && drawerBuilder.isDrawerOpen()) {
            drawerBuilder.closeDrawer();
        } else {
            this.finish();
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_about:
                Toast.makeText(Home.this, "PhotoLoader, Version 2.2.0", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_search:
                if (isSearchOpened) {

//                    edtSeach.requestFocus();
                    closeSearchBar();
                } else handleMenuSearch();
                return true;
        }

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_about) {
//            Toast.makeText(Home.this, "PhotoLoader, Version 2.0", Toast.LENGTH_SHORT).show();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void closeSearchBar() {
        action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
        action.setDisplayShowTitleEnabled(true); //show the title in the action bar

        //hides the keyboard
        View view = Home.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        //add the search icon in the action bar
        mSearchAction.setIcon(getResources().getDrawable(R.mipmap.ic_search_white_24dp));

        isSearchOpened = false;
    }

    private void handleMenuSearch() {
        action = getSupportActionBar(); //get the actionbar

//        if (isSearchOpened) { //test if the search is open
//
//            isSearchOpened = false;
//
//            View view = Home.this.getCurrentFocus();
//            if (view != null) {
//                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//            }
//
//            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
//            action.setDisplayShowTitleEnabled(true); //show the title in the action bar
//
//
//            mSearchAction.setIcon(getResources().getDrawable(R.mipmap.ic_search_white_24dp));
//        } else { //open the search entry

        action.setDisplayShowCustomEnabled(true); //enable it to display a
        // custom view in the action bar.
        action.setCustomView(R.layout.search_bar);//add the custom view
        action.setDisplayShowTitleEnabled(false); //hide the title

        edtSeach = (EditText) action.getCustomView().findViewById(R.id.edtSearch); //the text editor

        //this is a listener to do a search when the user clicks on search button
        edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String text = String.valueOf(edtSeach.getText());
                    closeSearchBar();
//                        isSearchOpened = false;
//
//                        View view = Home.this.getCurrentFocus();
//                        if (view != null) {
//                            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                        }
//
//                        action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
//                        action.setDisplayShowTitleEnabled(true); //show the title in the action bar
//
//
//                        mSearchAction.setIcon(getResources().getDrawable(R.mipmap.ic_search_white_24dp));
                    doSearch(text);
                    return true;
                }
                return false;
            }
        });


        edtSeach.requestFocus();

        //open the keyboard focused in the edtSearch
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);
        //add the close icon
        mSearchAction.setIcon(getResources().getDrawable(R.mipmap.ic_close));

        isSearchOpened = true;
//        }
    }

    private void doSearch(String text) {


        initGridViewSearch(); // вьюха созадан
        getImageLinksFromWeb(text); // список ссылок
        addDataToSearchGridView();
        //searchGridView.invalidateViews();
        //Toast.makeText(Home.this, text, Toast.LENGTH_SHORT).show();
    }

    private void getImageLinksFromWeb(String text) {
        list = new ArrayList<>();
        queue = Volley.newRequestQueue(this);
        final String[] query = text.split(" ");
        builder = null;
        progressDialog = new ProgressDialog(Home.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading... Please wait");
        //progressDialog.setTitle("Message");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        for (int i = 0; i < query.length; i++) {
            final int j = i;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    final String url = "https://www.flickr.com/search/?text=" + query[j];
                    Log.v("myLog", "Запуск запроса url");
                    sendImageRequest(url);
                    Log.v("myLog", "Конец запроса");
                }
            });

            thread.start();

        }

        if (query.length > 1) {
            String url = "https://www.flickr.com/search/?text=";
            for (int i = 0; i < query.length; i++) {
                url += query[i] + "%20";
            }
            url = url.substring(0, url.length() - 3);

            sendImageRequest(url);

            Log.v("myLog", "url = " + url);
            Log.v("myLog", "Запуск запроса");
        }

    }

    private void sendImageRequest(String url) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        list.addAll(getImageLinks(response));
//                        list = getImageLinks(response);
                        Log.v("myLog", "size = " + list.size());
                        Home.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageAdapter.notifyDataSetChanged();
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            }
                        });
                        removeDublicates(list);
                        Log.v("myLog", "Пришел ответ");
                        Log.v("myLog", "without dublicates size = " + list.size());
                        //txtDisplay.setText(Html.fromHtml(response.toString()));
                        //Log.v("myLog", response);
                    }

                    private void removeDublicates(List<String> list) {

                        Set uniqueEntries = new HashSet();
                        for (Iterator iter = list.iterator(); iter.hasNext(); ) {
                            Object element = iter.next();
                            if (!uniqueEntries.add(element)) // if current element is a duplicate,
                                iter.remove();                 // remove it
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (list.size() < 1 ){
                    if (builder == null) {
                        builder = new AlertDialog.Builder(Home.this);
                        builder.setPositiveButton("Ok", null);
                        builder.setMessage("Nothing to show");
                        builder.setCancelable(true);
                        builder.show();
                    }
                }

//                    AlertDialog alertDialog = builder.create();

                Log.v("myLog", "That didn't work!");
            }
        });
        queue.add(stringRequest);
        Log.v("myLog", "Конец запроса");
    }


    private List<String> getImageLinks(String response) {
        String line;
        Scanner scanner = new Scanner(response);
        List<String> list = new ArrayList<>();
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();

            if (line.contains("img.src=")) {
                //Log.v("myLog", line); // img.src='//c1.staticflickr.com/9/8298/7861351302_a9fef5f3b0_m.jpg';
                String result = line.substring(line.indexOf('\'') + 3, line.lastIndexOf('\''));
                list.add(result);
                //Log.v("myLog", "Result = ------------> " + result);
            }

        }
        return list;
    }

    private void addDataToSearchGridView() {
        imageAdapter = new ImageAdapter(Home.this);
        searchGridView.setAdapter(imageAdapter);
        searchGridView.setNumColumns(GridView.AUTO_FIT);
        searchGridView.setColumnWidth(200);

        searchGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                //Toast.makeText(Home.this, parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                saveImageOnDevice(position);


            }

            private void saveImageOnDevice(int position) {

                ImageLoader imageLoader = ImageLoader.getInstance();

                imageLoader.loadImage("http://" + list.get(position), new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        setMainFragment();

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        Log.v("myLog", "imageUri ------------------>" + imageUri);
                        File file = createCameraTmpFile();
                        Log.v("myLog", file.getAbsolutePath() + " ------------------>");
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                        FileOutputStream fo = null;

                        try {
                            fo = new FileOutputStream(file);
                            fo.write(bytes.toByteArray());
                            fo.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

//                        Uri uri = Uri.parse(imageUri);
                        Uri uri = Uri.parse("file://" + file.getPath());
                        Log.v("myLog", "Uri ------------------>" + uri.toString());
                        decodeAndViewImage(uri);
                        imageOpenGlobalUri = uri;
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });
//                Bitmap bimage = imageLoader.loadImageSync("http://" + list.get(position));


            }
        });

        imageAdapter.notifyDataSetChanged();
    }

    private void initGridViewSearch() {
        imageOpenGlobalUri = null;
        bitmap = null;
        if (searchFragment == null) {
            Log.v("myLog", "New Search Fragment");
            searchFragment = new SearchFragment();
        } else {
            Log.v("myLog", "Clear List");
            list.clear();
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.homeContainer, searchFragment, SEARCH_TAG);
        transaction.commit();
        fragmentManager.executePendingTransactions();

        View view = fragmentManager.findFragmentByTag(SEARCH_TAG).getView();
        //if (view != null) {
        Log.v("myLog", "View");
        searchGridView = (GridView) view.findViewById(R.id.gridview_search);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//
//                case RequestCode.REQUEST_CODE_CAMERA_SCREENSHOT:
//
//                    Uri uri2 = fileUri;
//                    Log.v(TAG + "ActivityResultUri ", uri2.toString());
//
////                    if (uri2 != null) {
////                        String path = uri2.toString();
////                        if (path.toLowerCase().startsWith("file://")) {
////                            // Selected file/directory path is below
////                            path = (new File(URI.create(path))).getAbsolutePath();
////                            Log.i(TAG + "LOG", "Pick completed: " + path);
////                        }
////                    } else Log.e(TAG + "LOG", "URI2 = : NULL");
////                    try {
////                        // We need to recyle unused bitmaps
////                        if (bitmap != null) {
////                            bitmap.recycle();
////                        }
////                        InputStream stream = getContentResolver().openInputStream(uri2);
////
////
////                        bitmap = BitmapFactory.decodeStream(stream);
////                        stream.close();
////
////
////                        imageView.setImageBitmap(bitmap);
////                    } catch (FileNotFoundException e) {
////                        Log.i(TAG + "LOG", "File not Founded");
////                        e.printStackTrace();
////                    } catch (IOException e) {
////                        e.printStackTrace();
////                    }
//                    // TODO Make the same in refact branch
//                    decodeAndViewImage(fileUri);
//                    File file2 = createCameraTmpFile(); // MyCameraApp - File
//                    uri3 = Uri.fromFile(file2);
//                    makeSmallSizeImage(file2, uri3);
//                    imageOpenGlobalUri = uri3;
////                    buttonShare.setEnabled(true);
////                    buttonUpload.setEnabled(true);
//                    break;
//
//                case RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE:
//
//                    decodeAndViewImage(data.getData());
//                    imageOpenGlobalUri = Uri.parse(data.getData().toString());
//                    Log.v("myLog", "Add link = " + imageOpenGlobalUri);
//
//                    break;
//            }
//        }

        switch (resultCode) {
            case RESULT_OK: {
                switch (requestCode) {

                    case RequestCode.REQUEST_CODE_CAMERA_SCREENSHOT:

                        Uri uri2 = fileUri;
                        Log.v(TAG + "ActivityResultUri ", uri2.toString());

//                    if (uri2 != null) {
//                        String path = uri2.toString();
//                        if (path.toLowerCase().startsWith("file://")) {
//                            // Selected file/directory path is below
//                            path = (new File(URI.create(path))).getAbsolutePath();
//                            Log.i(TAG + "LOG", "Pick completed: " + path);
//                        }
//                    } else Log.e(TAG + "LOG", "URI2 = : NULL");
//                    try {
//                        // We need to recyle unused bitmaps
//                        if (bitmap != null) {
//                            bitmap.recycle();
//                        }
//                        InputStream stream = getContentResolver().openInputStream(uri2);
//
//
//                        bitmap = BitmapFactory.decodeStream(stream);
//                        stream.close();
//
//
//                        imageView.setImageBitmap(bitmap);
//                    } catch (FileNotFoundException e) {
//                        Log.i(TAG + "LOG", "File not Founded");
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                        // TODO Make the same in refact branch
                        decodeAndViewImage(fileUri);
                        File file2 = createCameraTmpFile(); // MyCameraApp - File
                        uri3 = Uri.fromFile(file2);
                        makeSmallSizeImage(file2, uri3);
                        imageOpenGlobalUri = uri3;
//                    buttonShare.setEnabled(true);
//                    buttonUpload.setEnabled(true);
                        break;

                    case RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE:

                        decodeAndViewImage(data.getData());
                        imageOpenGlobalUri = Uri.parse(data.getData().toString());
                        Log.v("myLog", "Add link = " + imageOpenGlobalUri);

                        break;
                }

                break;
            }

            case STATUS_START: {
                switch (requestCode) {
                    case SERVICE_CODE:
                        Log.v("myLog", "Start Service");
                        break;
                }
                break;
            }

            case STATUS_IN_PROGRESS: {
                switch (requestCode) {
                    case SERVICE_CODE:
                        break;
                }
                break;
            }

            case STATUS_FINISH: {
                switch (requestCode) {
                    case SERVICE_CODE:
                        Log.v("myLog", "STATUS_FINISH");
                        String str = data.getStringExtra(PARAM_RESULT);
                        Log.v("myLog", "Value = " + str);
                        stopService(servIntent);
                        break;
                }
                break;
            }

            case STATUS_INIT: {
                Log.v("myLog", "STATUS_INIT");
                pd = new ProgressDialog(Home.this);
                pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pd.setMessage("Uploading Picture...");
                pd.setCancelable(false);
                pd.show();
                break;
            }

            case STATUS_ALERT: {
                String str = data.getStringExtra(PARAM_RESULT);
                urlImage = createAlertUrlDialog(str);
                urlImage.show();
                Log.v("myLog", "STATUS_ALERT");
                pd.cancel();
                break;
            }

            case STATUS_PROGRESS_UPDATE: {

                int val = data.getIntExtra(PARAM_UPDATE_VALUE, 0);
                Log.v("myLog", "STATUS_PROGRESS_UPDATE " + val);
                pd.setProgress(val);
                if (val >= 98) {
                    pd.setProgress(100);
                }
                break;
            }
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private AlertDialog.Builder createAlertUrlDialog(final String result) {
        return new AlertDialog.Builder(Home.this)
                .setTitle("Image Url")
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNeutralButton("Share", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(android.content.Intent.EXTRA_TEXT, result);
                        startActivity(Intent.createChooser(intent, "Share Url"));
                    }
                })
                .setPositiveButton("Open", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(result));

                        startActivity(i);
                    }
                })
                .setMessage(result);
    }

    private void initialiseNavigationDrawer() {

        AccountHeader result = createAccountHeader();

        drawerBuilder = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolBar)
//                .withDisplayBelowToolbar(true)
                .withDisplayBelowStatusBar(true)
                .withAnimateDrawerItems(true)
                .withActionBarDrawerToggleAnimated(true)
                .withAccountHeader(result)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withIdentifier(1)
                                .withName(R.string.nav_menu_item_gridview)
                                .withIcon(R.mipmap.ic_view_dashboard_black_18dp),

                        new DividerDrawerItem(),

                        new SecondaryDrawerItem()
                                .withIdentifier(2)
                                .withName(R.string.nav_menu_item_open)
                                .withIcon(R.mipmap.ic_description_black_18dp),

                        new SecondaryDrawerItem()
                                .withIdentifier(3)
                                .withName(R.string.nav_menu_item_camera)
                                .withIcon(R.mipmap.ic_camera_enhance_black_18dp),

                        new DividerDrawerItem(),

                        new SecondaryDrawerItem()
                                .withIdentifier(4)
                                .withName(R.string.nav_menu_item_upload)
                                .withIcon(R.mipmap.ic_upload_black_18dp),

                        new SecondaryDrawerItem()
                                .withIdentifier(5)
                                .withName(R.string.nav_menu_item_share)
                                .withIcon(R.mipmap.ic_share_black_18dp)

                )

                .withOnDrawerItemClickListener(addOnDrawerItemClickListener())
                .build();
    }

    private Drawer.OnDrawerItemClickListener addOnDrawerItemClickListener() {
        return new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                switch (iDrawerItem.getIdentifier()) {
                    case 1:
//                        Intent intent = new Intent(Home.this, SQLiteDBView.class);
//                        intent.putExtra("operation", "history");
//                        startActivity(intent);
                        bitmap = null;
                        SQLiteImageDB sqLiteImageDB = new SQLiteImageDB(Home.this);
                        items = sqLiteImageDB.readFromDB();
                        id = sqLiteImageDB.readIdFromDB();

                        initGridViewHistory();

                        imageHistoryAdapter = new ImageHistoryAdapter(Home.this, items, id);
                        historyGridView.setAdapter(imageHistoryAdapter);
                        historyGridView.setNumColumns(GridView.AUTO_FIT);
                        historyGridView.setColumnWidth(200);

                        historyGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View v,
                                                    int position, long id) {

                                setMainFragment();
                                imageOpenGlobalUri = Uri.parse(items[position]);
                                decodeAndViewImage(imageOpenGlobalUri);

                            }

                        });

                        historyGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                            @Override
                            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long idd) {
                                SQLiteImageDB sqLiteImageDB = new SQLiteImageDB(Home.this);
                                sqLiteImageDB.deleteById(id[position]);
                                id = ArrayUtils.remove(id, position);
                                items = ArrayUtils.remove(items, position);
                                //removeIntElement(id_image, position);
                                //removeStringElement(path, position);
                                ImageHistoryAdapter imageHistoryAdapter = new ImageHistoryAdapter(Home.this, items, id);
                                historyGridView.setAdapter(imageHistoryAdapter);
                                imageHistoryAdapter.notifyDataSetChanged();
                                Toast.makeText(Home.this, "Image was deleted", Toast.LENGTH_SHORT).show();
//                for (int i = 0; i < id_image.length; i++) {
//                    Log.v("myLog", "id = " + id_image[i]);
//                }
                                //gridview.invalidateViews();
                                return true;
                            }
                        });

                        imageHistoryAdapter.notifyDataSetChanged();

//                        Intent intent = new Intent(Home.this, GridViewImage.class);
//                        intent.putExtra("data", items);
//                        intent.putExtra("id", id);
//                        startActivity(intent);
                        break;
                    case 2:
                        Log.v("myLog", "Click open Button");
                        UtilMetods.simpleOpen = false;

                        setMainFragment();

                        addOpenImageIntent();
                        break;
                    case 3:
                        UtilMetods.simpleOpen = false;

                        setMainFragment();

                        addCameraIntent();
                        break;
                    case 4:
                        if (UtilMetods.isOnline()) {

                            if (imageOpenGlobalUri != null) {
                                decodeAndViewImage(imageOpenGlobalUri); // We must to upload already decoded image!!
                                saveDecodedImage();


                                addToDBLink(imageOpenGlobalUri.toString());
                                Intent tmp = new Intent();
                                pi = createPendingResult(SERVICE_CODE, tmp, 0);
                                servIntent = new Intent().putExtra(PARAM_PINTENT, pi);
                                servIntent.setAction("myservice.uploadservice");
                                sPref = PreferenceManager.getDefaultSharedPreferences(Home.this);
                                SharedPreferences.Editor ed = sPref.edit();
                                ed.putString("imageOpenGlobalUri", imageOpenGlobalUri.toString());
                                ed.putString("serviceIntent", servIntent.toURI());
                                ed.commit();
                                startService(servIntent);

//                                asyncClass = new AsyncClass();
//                                asyncClass.link(Home.this);
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                                    asyncClass.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
//                                } else {
//                                    asyncClass.execute((Void) null);
//                                }


                            } else
                                Toast.makeText(Home.this, "Please, pick image at first", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(Home.this, "Network is unreachable", Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
//                        if (UtilMetods.isOnline()) {
                        addShareIntent();
//                        }
//                        else Toast.makeText(ImagePickActivity.this,"Network is unreachable", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        };
    }

    private void initGridViewHistory() {
        imageOpenGlobalUri = null;
        bitmap = null;
        if (historyFragment == null) {
            Log.v("myLog", "New Search Fragment");
            historyFragment = new HistoryFragment();
        } else {

            Log.v("myLog", "Clear List");
            //list.clear();
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.homeContainer, historyFragment, HISTORY_TAG);
        transaction.commit();
        fragmentManager.executePendingTransactions();

        View view = fragmentManager.findFragmentByTag(HISTORY_TAG).getView();
        //if (view != null) {
        Log.v("myLog", "View");
        historyGridView = (GridView) view.findViewById(R.id.gridview);

    }

    private AccountHeader createAccountHeader() {
        IProfile profile = new ProfileDrawerItem()
                .withName("PhotoLoader");
        //.withEmail("Sasha1220_93@mail.ru")
        //.withIcon(getResources().getDrawable(R.mipmap.ic_verified_user_black_18dp));

        return new AccountHeaderBuilder()
                .withActivity(this)
                .withProfileImagesVisible(false)
                .withHeaderBackground(R.drawable.android_lollipop_material)
//                .withHeaderBackground(R.drawable.backgroud_matherial)
                        //.addProfiles(profile)
                .build();
    }

    private void decodeAndViewImage(final Uri uri) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (bitmap != null && bitmap.isRecycled()) {
                    bitmap.recycle();
                    bitmap = null;
                }
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                InputStream stream = null;

                try {
                    stream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                imageUri = uri;
                bitmap = BitmapFactory.decodeStream(stream, null, options);

                try {
                    if (stream != null) {
                        stream.close();
                    } else Log.v("myLog", "Stream is NULL");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                InputStream stream2 = null;

                try {
                    stream2 = getContentResolver().openInputStream(
                            uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                String imageType = options.outMimeType;
                Log.v("myLog", "Image Height = " + String.valueOf(imageHeight));
                Log.v("myLog", "Image Width = " + String.valueOf(imageWidth));
                Log.v("myLog", "Image type = " + String.valueOf(imageType));
                Display display = getWindowManager().getDefaultDisplay();
                int width = display.getWidth();
                int height = display.getHeight();
//                options.inSampleSize = 2 * calculateInSampleSize(options, width, height);
                options.inSampleSize = calculateInSampleSize(options, width, height);
                Log.v("myLog", "options.inSampleSize = " + String.valueOf(options.inSampleSize));
                options.inJustDecodeBounds = false;
                bitmap = BitmapFactory.decodeStream(stream2, null, options);

                try {
                    if (stream2 != null) {
                        stream2.close();
                    } else Log.v("myLog", "Stream2 is NULL");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();


        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        View view1 = mainFragment.getView();


        imageViewArea = (ImageView) view1.findViewById(R.id.imageViewArea);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageViewArea.setImageBitmap(bitmap);

            }
        });
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.v("myLog", "Image Height calculateInSampleSize = " + String.valueOf(height));
        Log.v("myLog", "Image Width calculateInSampleSize = " + String.valueOf(width));

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
            return 2 * inSampleSize;
        }

        return inSampleSize;
    }

    public Uri getImageOpenGlobalUri() {
        return imageOpenGlobalUri;
    }

    private void addOpenImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE);
    }

    private void addCameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(RequestCode.MEDIA_TYPE_IMAGE); // create a file to save the image
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        Log.v(TAG + "makeCameraScreen", fileUri.toString());
        // start the image capture Intent
        startActivityForResult(cameraIntent, RequestCode.REQUEST_CODE_CAMERA_SCREENSHOT);
    }

    private void addShareIntent() {
        if (bitmap != null) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            startActivity(Intent.createChooser(share, "Share Image"));
        } else Toast.makeText(Home.this, "Please, pick image at first", Toast.LENGTH_SHORT).show();

    }

    private static Uri getOutputMediaFileUri(int type) {
        Log.v(TAG + "MyCameraApp Uri", Uri.fromFile(getOutputMediaFile(type)).toString());
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "MyCameraApp");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG + "MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == RequestCode.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
            Log.v(TAG + "Path = ", mediaFile.getPath());
            Log.v(TAG + "Path URI = ", mediaFile.toURI().toString());
        } else {
            return null;
        }

        return mediaFile;
    }

    private File createCameraTmpFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "MyCameraApp");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG + "MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        Date data = new Date();
        Log.v(TAG, "" + data.getTime());
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
        Log.v(TAG + "Path = ", mediaFile.getPath());
        Log.v(TAG + "Path URI = ", mediaFile.toURI().toString());
        return mediaFile;
    }

    private void makeSmallSizeImage(File file, Uri uri2) {
        decodeFile(uri2, file);
    }

    private void decodeFile(Uri uri, File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;


        InputStream stream = null;

        try {
            stream = getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap bitmapSaved = BitmapFactory.decodeStream(stream, null, options);

        try {
            if (stream != null) {
                stream.close();
            } else Log.v(TAG + " ", "Stream is NULL");
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream stream2 = null;

        try {
            stream2 = getContentResolver().openInputStream(
                    uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;
        Log.v(TAG, "Image Height = " + String.valueOf(imageHeight));
        Log.v(TAG, "Image Width = " + String.valueOf(imageWidth));
        Log.v(TAG, "Image type = " + String.valueOf(imageType));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
//                display.getSize(size); //TODO SIZE or use anouther method 2.3.7 error, requred API LVL 13
//
//                int width = size.x;
//                int height = size.y;
        int width = display.getWidth();
        int height = display.getHeight();

        options.inSampleSize = calculateInSampleSize(options, width, height);
        Log.v(TAG, "options.inSampleSize = " + String.valueOf(options.inSampleSize));
        options.inJustDecodeBounds = false;

        bitmapSaved = BitmapFactory.decodeStream(stream2, null, options);


        OutputStream fOutputStream = null;
//        File filetmp = new File(Environment.getExternalStorageDirectory() + "/", "tmp.jpg");

        try {
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            if (stream2 != null) {
                stream2.close();
            } else Log.v(TAG, "Stream2 is NULL");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveDecodedImage() {
        File file = new File(Environment.getExternalStorageDirectory() + "/tmp.jpg");

        if (file.exists()) {
            file.delete();
        }

        try {
            if (file.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        OutputStream fOutputStream = null;
        File filetmp = new File(Environment.getExternalStorageDirectory() + "/", "tmp.jpg");

        try {
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), filetmp.getAbsolutePath(), filetmp.getName(), filetmp.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addToDBLink(String filePath) {

        SQLiteImageDB sqLiteImageDB = new SQLiteImageDB(Home.this);
        sqLiteImageDB.addToDB(filePath);
//        Intent intent = new Intent(Home.this, SQLiteDBView.class);
//        intent.putExtra("operation", "add"); // Просмотр базы при нажатии на кнопку
//        intent.putExtra("path", filePath);
//        startActivity(intent);
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;


        public ImageAdapter(Context c) {
            mContext = c;
            initImageLoader(mContext);
        }

        public void initImageLoader(Context context) {
            // This configuration tuning is custom. You can tune every option, you may tune some of them,
            // or you can create default configuration by
            //  ImageLoaderConfiguration.createDefault(this);
            // method.
            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
            config.discCacheFileNameGenerator(new Md5FileNameGenerator());
            config.discCacheSize(50 * 1024 * 1024);
            config.tasksProcessingOrder(QueueProcessingType.FIFO);
            config.writeDebugLogs(); // Remove for release app

            // Initialize ImageLoader with configuration.
            ImageLoader.getInstance().init(config.build());
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 4, 4, 4);
            } else {
                imageView = (ImageView) convertView;
            }
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageSize imageSize = new ImageSize(200, 200);
            BitmapFactory.Options bitopOptions = new BitmapFactory.Options();
            bitopOptions.outWidth = 200;
            bitopOptions.outHeight = 200;

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .resetViewBeforeLoading(false)  // default
                    .delayBeforeLoading(100)
                    .showImageOnLoading(R.drawable.start)
                    .considerExifParams(false) // default
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                    .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                    .cacheOnDisc(true)
                    .decodingOptions(bitopOptions)
                    .build();
            String link = "http://" + list.get(position);

            //Log.v("myLog", "link = " + link);
            //imageView.setImageResource(R.drawable.start);
            imageLoader.displayImage(link, imageView, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    //Log.v(TAG + "TAG", "End");
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
//            imageLoader.displayImage(list.get(position), imageView, options);
            //imageView.setImageResource(R.drawable.start);
            return imageView;
        }
    }

    class ImageHistoryAdapter extends BaseAdapter {
        private Context mContext;
        private String[] items;
        private int[] id;

        public ImageHistoryAdapter(Context mContext, String[] items, int[] id) {
            this.mContext = mContext;
            this.items = items;
            this.id = id;
            initImageLoader(mContext);
        }

        public void initImageLoader(Context context) {
            // This configuration tuning is custom. You can tune every option, you may tune some of them,
            // or you can create default configuration by
            //  ImageLoaderConfiguration.createDefault(this);
            // method.
            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
            config.discCacheFileNameGenerator(new Md5FileNameGenerator());
            config.discCacheSize(50 * 1024 * 1024);
            config.tasksProcessingOrder(QueueProcessingType.FIFO);
            config.writeDebugLogs(); // Remove for release app

            // Initialize ImageLoader with configuration.
            ImageLoader.getInstance().init(config.build());
        }

        @Override
        public int getCount() {
            return items.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ImageView imageView;
            if (convertView == null) {
                // if it's not recycled, initialize some attributes
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 4, 4, 4);
            } else {
                imageView = (ImageView) convertView;
            }
            ImageLoader imageLoader = ImageLoader.getInstance();
            ImageSize imageSize = new ImageSize(200, 200);
            BitmapFactory.Options bitopOptions = new BitmapFactory.Options();
            bitopOptions.outWidth = 200;
            bitopOptions.outHeight = 200;

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .resetViewBeforeLoading(false)  // default
                    .delayBeforeLoading(100)
                    .showImageOnLoading(R.drawable.start)
                    .considerExifParams(false) // default
                    .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                    .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                    .cacheOnDisc(true)
                    .decodingOptions(bitopOptions)
                    .build();
            if (items[position].contains("MyCameraApp")) {
                imageLoader.loadImage(items[position], options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        Log.v("myLog", "Start Load Image");
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        Log.v("myLog", "Failed Load Image " + imageUri + " " + failReason.getCause().toString());
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, final Bitmap loadedImage) {
                        Log.v("myLog", "Complete Load Image " + imageUri);
                        Home.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                imageView.setImageBitmap(loadedImage);
                            }
                        });

                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        Log.v("myLog", "Cancel Load Image " + imageUri);
                    }
                });

            } else imageLoader.displayImage(items[position], imageView, options);

            return imageView;
        }
        //TODO some problem with camera's photo
        // references to our images
        //private Integer[] mThumbIds;
    }


//    @Override
//    public Object onRetainCustomNonConfigurationInstance() {
//        asyncClass.unLink();
//        return asyncClass;
//    }

//    static class AsyncClass extends AsyncTask<Void, Integer, Void> {
//
//        Home home;
//
//
//        void link(Home home) {
//            this.home = home;
//        }
//
//        void unLink() {
//            home = null;
//        }
//
//
//        private int progress = 10;
//        //NotificationCompat.Builder notification;
////        Notification notification;
////        NotificationManager notificationManager;
//
//
//        AlertDialog.Builder urlImage;
//        Intent serviceIntent;
//        long totalSize;
//
//        @Override
//        protected void onPreExecute() {
//            home.pd = new ProgressDialog(home);
//            home.pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            home.pd.setMessage("Uploading Picture...");
//            home.pd.setCancelable(false);
//
//
//            //notification = new NotificationCompat.Builder(getApplicationContext());
//
//
////            notification = new Notification(R.drawable.notification_template_icon_bg, "simulating a download", System
////                    .currentTimeMillis());
////            notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;
////            notification.contentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.download_progress);
////            notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.abc_ic_menu_paste_mtrl_am_alpha);
////            notification.contentView.setTextViewText(R.id.status_text, "simulation in progress");
////            notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);
//
////            notificationManager = (NotificationManager) getApplicationContext().getSystemService(
////                    getApplicationContext().NOTIFICATION_SERVICE);
//
////            notificationManager.notify(42, notification);
//
//
//            home.pd.show();
//
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            home.pd.cancel();
//
//            final ResultProcessing resultProcessing = new ResultProcessing();
//
//            urlImage = createAlertUrlDialog(resultProcessing);
//
//            serviceIntent = new Intent();
//            serviceIntent.setAction("services.notificationService");
//            home.startService(serviceIntent);
//
//            urlImage.show();
//
////            urlImage.setCanceledOnTouchOutside(true);
////            urlImage.setContentView(R.layout.urlimage);
////            urlImage.show();
////            Handler handler = new Handler();
////            handler.postDelayed(new Runnable() {
////                @Override
////                public void run() {
////                    urlImage.cancel();
////                }
////            },5000);
//
//            return;
//        }
//
//        private AlertDialog.Builder createAlertUrlDialog(final ResultProcessing resultProcessing) {
//            return new AlertDialog.Builder(home)
//                    .setTitle("Image Url")
//                    .setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            home.stopService(serviceIntent);
//                        }
//                    })
//                    .setPositiveButton("Open", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//
//                            String url = resultProcessing.getImageUrl(home.finalUrl);
//                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            i.setData(Uri.parse(url));
//                            home.stopService(serviceIntent);
//                            home.startActivity(i);
//                        }
//                    })
//                    .setMessage(resultProcessing.getImageUrl(home.finalUrl));
//        }
//
//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            super.onProgressUpdate(values);
//            //notification.contentView.setProgressBar(R.id.status_progress, 100, values[0], false);
//            //notificationManager.notify(42, notification);
//            home.pd.setProgress(values[0]);
//            if (values[0] >= 98) {
//                home.pd.setProgress(100);
//                //notification.contentView.setProgressBar(R.id.status_progress, 100, values[0], true);
//                //notificationManager.notify(42, notification);
//            }
//        }
//
//        @Override
//        protected Void doInBackground(Void... params) {
//
//            home.decodeAndViewImage(home.imageOpenGlobalUri); // We must to upload already decoded image!!
//            home.saveDecodedImage();
//            File file = new File(Environment.getExternalStorageDirectory() + "/tmp.jpg");
//
//
//            final String absFilePath = file.getAbsolutePath();
//            //HttpResponse response = null;
//
//
//            String textFile = absFilePath;
//            Log.v(TAG + "TAG", "textFile: " + textFile);
//
//            // the URL where the file will be posted
//            String postReceiverUrl = "http://deviantsart.com/upload.php";
//            Log.v(TAG + "TAG", "postURL: " + postReceiverUrl);
//
//            // new HttpClient
//            HttpClient httpClient = new DefaultHttpClient();
//
//            // post header
//            HttpPost httpPost = new HttpPost(postReceiverUrl);
//
//            file = new File(textFile);
//            if (file.exists()) {
//                Log.v(TAG + "TAG", "File exists");
//            }
//            FileBody fileBody = new FileBody(file);
//            home.totalLength = fileBody.getContentLength();
//            CustomMultiPartEntity reqEntity = new CustomMultiPartEntity(
//                    HttpMultipartMode.BROWSER_COMPATIBLE, new CustomMultiPartEntity.ProgressListener() {
//                int lastPercent = 0;
//
//                @Override
//                public void transferred(long num) {
//
//                    home.percentUploaded = (int) (((float) num) / ((float) home.totalLength) * 99f);
//                    try {
//                        TimeUnit.MILLISECONDS.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    if (num <= home.totalLength) {
//
//                        home.percentUploaded = (int) (((float) num * 99f) / ((float) home.totalLength));
////                                pd.setProgress(percentUploaded);
//                        Log.d("myLog", "percent uploaded: " + home.percentUploaded + " - " + num + " / " + home.totalLength);
//                        progress = home.percentUploaded;
//                        publishProgress(home.percentUploaded);
////                                notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);
////                                notificationManager.notify(42, notification);
//                        //lastPercent = percentUploaded;
//                    }
//                    if (home.percentUploaded >= 98) {
//                        publishProgress(100);
//                    }
//
//                }
//
//            });
//
//            //MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//            reqEntity.addPart("file", fileBody);
//            httpPost.setEntity(reqEntity);
//
//            // execute HTTP post request
//
//            try {
//                home.response = httpClient.execute(httpPost);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            HttpEntity resEntity = home.response.getEntity();
//            if (resEntity != null) {
//
//
//                try {
//                    home.responseStr = EntityUtils.toString(resEntity).trim();
//                    Log.v("myLog", "Response: " + home.responseStr);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//
//            file.delete();
//            Log.v("myLog", home.imageOpenGlobalUri.getEncodedAuthority() + " " + home.imageOpenGlobalUri.getEncodedPath());
//            home.addToDBLink(home.imageOpenGlobalUri.toString());
//            home.pd.cancel();
////            notificationManager.cancel(42);
//            ResultProcessing resultProcessing = new ResultProcessing();
//            home.finalUrl = home.responseStr;
//            Log.v(TAG + " = ", resultProcessing.getImageUrl(home.finalUrl));
//            // TODO FIX TO UPLOAD SMALL SIZE IMAGE, May be tmp. Origin image will download now.
//            return null;
//        }
//
//
//    }


}
