package com.android.giffel.photoloader;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;

public class UploadImage extends Activity {

    private static final String TAG = "myLog";

    private HttpResponse response;
    private String finalResponseStr;
    private String responseStr;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.upload_activity);
        String path = getIntent().getStringExtra("path");

        uploadImage(path);
        Log.v(TAG + "LOG path = ", path);

        ResultProcessing resultProcessing = new ResultProcessing();
        finalResponseStr = responseStr;

        Intent urlIntent = new Intent();
        urlIntent.putExtra("imageUrl", resultProcessing.getImageUrl(finalResponseStr));
        setResult(RESULT_OK, urlIntent);
        finish();

    }

    private void uploadImage(String absoluteFilePath) {

        final String absFilePath = absoluteFilePath;
        //HttpResponse response = null;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                String textFile = absFilePath;
                Log.v(TAG + "TAG", "textFile: " + textFile);

                // the URL where the file will be posted
                String postReceiverUrl = "http://uploads.ru/upload.php";
                Log.v(TAG + "TAG", "postURL: " + postReceiverUrl);

                // new HttpClient
                HttpClient httpClient = new DefaultHttpClient();

                // post header
                HttpPost httpPost = new HttpPost(postReceiverUrl);

                File file = new File(textFile);
                if (file.exists()) {
                    Log.v(TAG + "TAG", "File exists");
                }
                FileBody fileBody = new FileBody(file);

                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                reqEntity.addPart("file", fileBody);
                httpPost.setEntity(reqEntity);

                // execute HTTP post request

                try {
                    response = httpClient.execute(httpPost);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {


                    try {
                        responseStr = EntityUtils.toString(resEntity).trim();
                        Log.v(TAG + "TAG!", "Response: " + responseStr);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //String jsonObject = responseStr;

//                final ResultProcessing resultProcessing = new ResultProcessing();
//                finalResponseStr = responseStr;
//                return resultProcessing.getImageUrl(finalResponseStr);

//                final String finalResponseStr = responseStr;

//                MyActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        editTextUrlResult.setText(resultProcessing.getImageUrl(finalResponseStr));
//                    }
//                });


                    // you can add an if statement here and do other actions based on the response
                }



            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}




