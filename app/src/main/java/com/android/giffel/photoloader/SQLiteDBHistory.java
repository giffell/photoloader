package com.android.giffel.photoloader;


import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class SQLiteDBHistory extends Activity{


    String[] mobileArray = {"Android","IPhone","WindowsMobile","Blackberry","WebOS","Ubuntu","Windows7","Max OS X"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sqlitedb_history_activity);

        String[] data = getIntent().getStringArrayExtra("data");
        ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.listview_activity, data);

        ListView listView = (ListView) findViewById(R.id.mobile_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("", "itemClick: position = " + position + ", id = "
                        + id);
                String str = (String) parent.getItemAtPosition(position);
                Toast toast = Toast.makeText(SQLiteDBHistory.this, str, Toast.LENGTH_SHORT);
                toast.show();

            }
        });

    }



    @Override
    public void onBackPressed() {
        finish();

    }




}
