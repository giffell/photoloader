package com.android.giffel.photoloader;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.filechooser.FileChooser;
import com.lessons.examples.ExampleActivity;
import com.lessons.examples.ViewPagerMain;

import java.io.File;
import java.net.URI;

import util.RequestCode;
import util.UtilMetods;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();


    private Button buttonStart;
    private Button buttonOpenImage;
    private Button buttonViewPager;
    private TextView textviewUrlResult;

    private String absoluteFilePath;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        buttonStart = (Button) findViewById(R.id.buttonStart);
        buttonOpenImage = (Button) findViewById(R.id.button_my);
        buttonViewPager = (Button) findViewById(R.id.buttonViewPager);

        textviewUrlResult = (TextView) findViewById(R.id.textViewResultUrl);
        textviewUrlResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textviewUrlResult.getText().toString().equals("Url")) {
                    Intent i = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(textviewUrlResult.getText().toString()));
                    startActivity(i);
                }
            }
        });




    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case RequestCode.REQUEST_CODE_OPEN_FILE:
                    absoluteFilePath = data.getStringExtra("path");
                    break;

                case RequestCode.REQUEST_CODE_UPLOAD:
                    final String imageUrl = data.getStringExtra("imageUrl");
                    URI uri = new File(imageUrl).toURI(); // TODO Uri
                    Log.v(TAG + " = ", uri.toString());

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textviewUrlResult.setText(imageUrl);
                        }
                    });
                    break;
            }
        }

        Log.v(TAG + " ", "File path  " + absoluteFilePath);

    }



    public void openFile(View view) {
        Intent intent = new Intent(this, FileChooser.class);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_OPEN_FILE);
    }

    public void uploadButtonListener(View view) {

        if (absoluteFilePath != null){
            if (UtilMetods.isOnline()) {
                Intent intent = new Intent(getApplicationContext(), UploadImage.class);
                intent.putExtra("path", absoluteFilePath);
                startActivityForResult(intent, RequestCode.REQUEST_CODE_UPLOAD);
            }
            else Toast.makeText(MainActivity.this,"Network is unreachable", Toast.LENGTH_SHORT).show();

        }
        else Toast.makeText(MainActivity.this,"Pick file at first", Toast.LENGTH_SHORT).show();
    }

    public void openImage(View view) {
        Intent intent = new Intent(getApplicationContext(), ImagePickActivity.class);
        startActivity(intent);
    }

    public void clickLessonsButtonListener(View view) {
        Intent intent = new Intent(MainActivity.this, ExampleActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            Toast.makeText(MainActivity.this, "HelloWorld", Toast.LENGTH_SHORT).show();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonViewPagerListener(View view) {
        startActivity(new Intent(MainActivity.this, ViewPagerMain.class));
    }

}
