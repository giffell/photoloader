package com.android.giffel.photoloader;


import android.app.Activity;
import android.os.Bundle;


public class SingleTouchImageViewActivity extends Activity {

    private TouchImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_touchimageview);
        //
        // DecimalFormat rounds to 2 decimal places.
        //
        image = (TouchImageView) findViewById(R.id.img);

        //
        // Set the OnTouchImageViewListener which updates edit texts
        // with zoom and scroll diagnostics.
        //
//        image.setOnTouchImageViewListener(new TouchImageView.OnTouchImageViewListener() {
//
//            @Override
//            public void onMove() {
//                image.getScrollPosition();
//                image.getZoomedRect();
//                image.getCurrentZoom();
//                image.isZoomed();
//            }
//        });
    }
}