package com.android.giffel.photoloader.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.android.giffel.photoloader.R;

public class HistoryFragment extends Fragment{

    private GridView gridview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.gridview_activity, container, false);
        gridview = (GridView) view.findViewById(R.id.gridview);

        return view;
    }
}
