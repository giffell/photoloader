package com.android.giffel.photoloader;

import android.util.Log;


public class ResultProcessing {

    private static final String TAG = ResultProcessing.class.getSimpleName();

    public String getImageUrl(String responseStr) {
        String result = "";

        try {
            String urlUploadImg = responseStr.substring(responseStr.lastIndexOf("<input type="), responseStr.lastIndexOf("[/img]"));
            Log.v(TAG, urlUploadImg);
            result = urlUploadImg.substring(urlUploadImg.indexOf("http"));

        } catch (Exception e) {

            Log.v(TAG, "ERROR!!!");
            if (result.equals("")) {
                result = responseStr.substring(responseStr.indexOf("\"") + 1, responseStr.lastIndexOf("\""));
                result = result.substring(10);
                result = "http://uploads.ru/" + result;
            }
        }
        Log.v(TAG + "Result imageUrl = ", result);
        return result;
    }
}
