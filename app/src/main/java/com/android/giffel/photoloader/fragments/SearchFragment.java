package com.android.giffel.photoloader.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.android.giffel.photoloader.R;

public class SearchFragment extends Fragment {

    private GridView gridview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.search_fragment, container, false);
        //gridview = (GridView) view.findViewById(R.id.gridview_search);
        Log.v("myLog", "Create GridSearch Fragment");
//        imageViewArea1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity().getApplicationContext(), SingleTouchImageViewActivity.class);
//                startActivity(intent);
//            }
//        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v("myLog", "Resume Fragment");
    }
}
