package com.android.giffel.photoloader.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.giffel.photoloader.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import util.RequestCode;

public class OpenFragment extends Fragment {
    ImageView imageView;
    private Uri imageUri;
    private Uri imageOpenGlobalUri;
    private Bitmap bitmap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.open_fragment, null);
        imageView = (ImageView) view.findViewById(R.id.iv_clickToOpen);
        imageView.setImageResource(R.drawable.clicktome);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addOpenImageIntent();
            }
        });
        return view;
    }

    private void addOpenImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {

                case RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE:
                    Log.v("myLog", "Opened");
                    decodeAndViewImage(data.getData());
                    imageOpenGlobalUri = Uri.parse(data.getData().toString());
                    Log.v("myLog", "Add link = " + imageOpenGlobalUri);

                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void decodeAndViewImage(final Uri uri) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {


                BitmapFactory.Options options = new BitmapFactory.Options();

                options.inJustDecodeBounds = true;


                InputStream stream = null;

                try {
                    stream = getActivity().getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                imageUri = uri;

                bitmap = BitmapFactory.decodeStream(stream, null, options);

                try {
                    if (stream != null) {
                        stream.close();
                    } else Log.v("myLog", "Stream is NULL");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                InputStream stream2 = null;

                try {
                    stream2 = getActivity().getContentResolver().openInputStream(
                            uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                String imageType = options.outMimeType;
                Log.v("myLog", "Image Height = " + String.valueOf(imageHeight));
                Log.v("myLog", "Image Width = " + String.valueOf(imageWidth));
                Log.v("myLog", "Image type = " + String.valueOf(imageType));

                Display display = getActivity().getWindowManager().getDefaultDisplay();
                Point size = new Point();
//                display.getSize(size); //TODO SIZE or use anouther method 2.3.7 error, requred API LVL 13
//
//                int width = size.x;
//                int height = size.y;
                int width=display.getWidth();
                int height=display.getHeight();

                options.inSampleSize = calculateInSampleSize(options, width, height);
                Log.v("myLog", "options.inSampleSize = " + String.valueOf(options.inSampleSize));
                options.inJustDecodeBounds = false;

                bitmap = BitmapFactory.decodeStream(stream2, null, options);
                try {
                    if (stream2 != null) {
                        stream2.close();
                    } else Log.v("myLog", "Stream2 is NULL");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();


        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ((ImageView) getView().findViewById(R.id.iv_clickToOpen)).setImageBitmap(bitmap);
        //imageView.setImageBitmap(bitmap);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.v("myLog", "Image Height calculateInSampleSize = " + String.valueOf(height));
        Log.v("myLog", "Image Width calculateInSampleSize = " + String.valueOf(width));

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public Uri getImageOpenGlobalUri() {
        return imageOpenGlobalUri;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
