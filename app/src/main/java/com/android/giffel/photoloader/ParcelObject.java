package com.android.giffel.photoloader;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ParcelObject implements Parcelable {

    public Bitmap bitmap;

    public ParcelObject(Bitmap bitmap) {
        Log.d("myLog", "ConstructorParcel");
        this.bitmap = bitmap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Log.d("myLog", "writeToParcel");
        dest.writeValue(bitmap);
    }

    public static final Parcelable.Creator<ParcelObject> CREATOR = new Parcelable.Creator<ParcelObject>() {
        // распаковываем объект из Parcel
        @Override
        public ParcelObject createFromParcel(Parcel in) {
            Log.d("myLog", "createFromParcel");
            return new ParcelObject(in);
        }

        @Override
        public ParcelObject[] newArray(int size) {
            return new ParcelObject[size];
        }
    };

    private ParcelObject(Parcel parcel) {
        Log.d("myLog", "readFromParcel");
        bitmap = (Bitmap) parcel.readValue(ClassLoader.getSystemClassLoader());
    }
}
