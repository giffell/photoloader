package com.android.giffel.photoloader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SQLiteImageDB {

    DBHelper dbHelper;
    Context context;

    public SQLiteImageDB(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
    }

    public String[] readFromDB() {
// создаем объект для данных
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        List<String> stockList = new ArrayList<String>();

        // получаем данные из полей ввода
        //Log.d(LOG_TAG, "--- Rows in imagetable: ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("imagetable", null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                //Log.d(LOG_TAG,"ID = " + c.getInt(idColIndex) + ", name = " + c.getString(nameColIndex));
                stockList.add(c.getString(nameColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            //Log.d(LOG_TAG, "0 rows");
            c.close();

        String[] stockArr = new String[stockList.size()];
        stockArr = stockList.toArray(stockArr);
        dbHelper.close();
        return stockArr;
    }

    public void addToDB(String filePath) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Log.d("myLog", "--- Insert in mytable: ---");
        // подготовим данные для вставки в виде пар: наименование столбца - значение

        cv.put("name", filePath);
        // вставляем запись и получаем ее ID
        long rowID = db.insert("imagetable", null, cv);
        Log.d("myLog", "row inserted, ID = " + rowID);
    }

    public int[] convertIntegers(List<Integer> integers)
    {
        int[] ret = new int[integers.size()];
        Iterator<Integer> iterator = integers.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().intValue();
        }
        return ret;
    }

    public int[] readIdFromDB() {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        List<Integer> stockList = new ArrayList<Integer>();
        // получаем данные из полей ввода
        //Log.d(LOG_TAG, "--- Rows in imagetable: ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("imagetable", null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                //Log.d(LOG_TAG,"ID = " + c.getInt(idColIndex) + ", name = " + c.getString(nameColIndex));
                stockList.add(c.getInt(idColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            //Log.d(LOG_TAG, "0 rows");
            c.close();

        int[] stockArr = convertIntegers(stockList);
        dbHelper.close();
        return stockArr;
    }

    public void deleteById(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("imagetable", "id = " + id, null);
        Log.v("myLog", "Deleted");
    }


    // readFromDB()
    // return array
    // start intent
//    public void getHistory(){
//        String[] arr = readFromDB();
//        Intent intent = new Intent(context, GridViewImage.class);
//        intent.putExtra("data", arr);
//        startActivity(intent);
//    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "imageDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("LOG_TAG", "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table imagetable ("
                    + "id integer primary key autoincrement,"
                    + "name text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
