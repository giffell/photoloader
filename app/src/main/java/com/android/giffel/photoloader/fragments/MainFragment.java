package com.android.giffel.photoloader.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.giffel.photoloader.R;

public class MainFragment extends Fragment{

    private ImageView imageViewArea1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.main_fragment, container, false);
        imageViewArea1 = (ImageView) view.findViewById(R.id.imageViewArea);
        imageViewArea1.setImageResource(R.drawable.start);
        Log.v("myLog", "Create Main Fragment");
//        imageViewArea1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getActivity().getApplicationContext(), SingleTouchImageViewActivity.class);
//                startActivity(intent);
//            }
//        });
        return view;
    }

    public View getViewFromFragnment() {
        return imageViewArea1;
    }


}
