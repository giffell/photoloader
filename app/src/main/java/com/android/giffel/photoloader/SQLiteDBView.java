package com.android.giffel.photoloader;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexander on 29.06.2015.
 */
public class SQLiteDBView extends Activity {

    final String LOG_TAG = "myLogs";


    static DBHelper dbHelper;
    private Button buttonHistory;
    private Button buttonGridView;
    private Button buttonDeleteAll;
    private Button buttonAddToHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sqlitedbview_activity);

        buttonHistory = (Button) findViewById(R.id.buttonHistory);
        buttonGridView = (Button) findViewById(R.id.buttonGridView);
        buttonDeleteAll = (Button) findViewById(R.id.buttonDeleteAll);
        buttonAddToHistory = (Button) findViewById(R.id.buttonAddToHistory);

        dbHelper = new DBHelper(this);
        String operation = "";
        operation = getIntent().getStringExtra("operation");

        switch (operation) {
            case ("add") : {
                String filePath = getIntent().getStringExtra("path");
                addToDB(filePath);
                finish();
                break;
            }
            case ("read") :{

                break;
            }
            case ("history"): {
                String[] arr = readFromDB();
                Intent intent = new Intent(SQLiteDBView.this, GridViewImage.class);
                intent.putExtra("data", arr);
                startActivity(intent);
                break;
            }
        }


    }

    public void addToDB(String filePath) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "--- Insert in mytable: ---");
        // подготовим данные для вставки в виде пар: наименование столбца - значение

        cv.put("name", filePath);
        // вставляем запись и получаем ее ID
        long rowID = db.insert("imagetable", null, cv);
        Log.d(LOG_TAG, "row inserted, ID = " + rowID);
    }

    public static String[] readFromDB() {
// создаем объект для данных
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        List<String> stockList = new ArrayList<String>();

        // получаем данные из полей ввода
        //Log.d(LOG_TAG, "--- Rows in imagetable: ---");
        // делаем запрос всех данных из таблицы mytable, получаем Cursor
        Cursor c = db.query("imagetable", null, null, null, null, null, null);

        // ставим позицию курсора на первую строку выборки
        // если в выборке нет строк, вернется false
        if (c.moveToFirst()) {

            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");

            do {
                // получаем значения по номерам столбцов и пишем все в лог
                //Log.d(LOG_TAG,"ID = " + c.getInt(idColIndex) + ", name = " + c.getString(nameColIndex));
                stockList.add(c.getString(nameColIndex));
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        } else
            //Log.d(LOG_TAG, "0 rows");
        c.close();

        String[] stockArr = new String[stockList.size()];
        stockArr = stockList.toArray(stockArr);
        dbHelper.close();
        return stockArr;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void buttonDeleteAllListener(View view) {
        ContentValues cv = new ContentValues();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "--- Clear mytable: ---");
        // удаляем все записи
        int clearCount = db.delete("imagetable", null, null);
        dbHelper.close();
        Log.d(LOG_TAG, "deleted rows count = " + clearCount);
    }

    public void buttonHistoryListener(View view) {
        String[] arr = readFromDB();
        Intent intent = new Intent(SQLiteDBView.this, SQLiteDBHistory.class);
        intent.putExtra("data", arr);
        startActivity(intent);
    }

    public void buttonGridViewListener(View view) {
        String[] arr = readFromDB();
        Intent intent = new Intent(SQLiteDBView.this, GridViewImage.class);
        intent.putExtra("data", arr);
        startActivity(intent);
    }

    public void buttonAddListener(View view) {
        String[] arr = readFromDB();
        for (int i = 0; i < 20; i++) {
            addToDB(arr[0]);
        }
    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            // конструктор суперкласса
            super(context, "imageDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(LOG_TAG, "--- onCreate database ---");
            // создаем таблицу с полями
            db.execSQL("create table imagetable ("
                    + "id integer primary key autoincrement,"
                    + "name text" + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
