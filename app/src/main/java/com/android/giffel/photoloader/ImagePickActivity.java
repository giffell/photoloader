package com.android.giffel.photoloader;


import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import util.RequestCode;
import util.UtilMetods;

public class ImagePickActivity extends AppCompatActivity {


    private HttpResponse response;
    private String finalResponseStr;
    private String responseStr;
    private static final String TAG = "myLog";

    private Bitmap bitmap;
    private ImageView imageView;
    private Button button;
    private Button buttonShare;
    private Button buttonCamera;
    private Button buttonUpload;
    private android.support.v7.widget.Toolbar toolBar;

    private Uri imageUri;
    private Uri fileUri;

    private Drawer drawerBuilder;
    private Uri imageOpenGlobalUri;

    private int percentUploaded = 0;
    private long totalLength = 292864;

    private boolean cameraImage = false;
    private Uri uri3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);
        imageView = (ImageView) findViewById(R.id.result);

        toolBar = (Toolbar) findViewById(R.id.toolbar1);
        if (toolBar != null) {
            setSupportActionBar(toolBar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }



//        button = (Button) findViewById(R.id.button1);
//        buttonShare = (Button) findViewById(R.id.buttonShare);
//        buttonCamera = (Button) findViewById(R.id.buttonCamera);
//        buttonUpload = (Button) findViewById(R.id.buttonUpload);
//        buttonShare.setEnabled(false);
//        buttonUpload.setEnabled(false);
        if (getIntent().getData() != null) decodeAndViewImage(getIntent().getData());



        initialiseNavigationDrawer();

    }

    private void initialiseNavigationDrawer() {

        AccountHeader result = createAccountHeader();

        drawerBuilder = new DrawerBuilder()
                .withActivity(ImagePickActivity.this)
                .withToolbar(toolBar)
                .withDisplayBelowToolbar(true)
                .withAnimateDrawerItems(true)
                .withActionBarDrawerToggleAnimated(true)
                .withAccountHeader(result)
                .addDrawerItems(
                        new PrimaryDrawerItem()
                                .withIdentifier(1)
                                .withName(R.string.nav_menu_item_gridview)
                                .withIcon(R.mipmap.ic_view_dashboard_black_18dp),

                        new DividerDrawerItem(),

                        new SecondaryDrawerItem()
                                .withIdentifier(2)
                                .withName(R.string.nav_menu_item_open)
                                .withIcon(R.mipmap.ic_description_black_18dp),

                        new SecondaryDrawerItem()
                                .withIdentifier(3)
                                .withName(R.string.nav_menu_item_camera)
                                .withIcon(R.mipmap.ic_camera_enhance_black_18dp),

                        new DividerDrawerItem(),

                        new SecondaryDrawerItem()
                                .withIdentifier(4)
                                .withName(R.string.nav_menu_item_upload)
                                .withIcon(R.mipmap.ic_upload_black_18dp),

                        new SecondaryDrawerItem()
                                .withIdentifier(5)
                                .withName(R.string.nav_menu_item_share)
                                .withIcon(R.mipmap.ic_share_black_18dp)

                )

                .withOnDrawerItemClickListener(addOnDrawerItemClickListener())
                .build();
    }

    private Drawer.OnDrawerItemClickListener addOnDrawerItemClickListener() {
        return new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                switch (iDrawerItem.getIdentifier()) {
                    case 1:
                        Intent intent = new Intent(ImagePickActivity.this, SQLiteDBView.class);
                        intent.putExtra("operation", "history"); // Просмотр базы при нажатии на кнопку
                        startActivity(intent);
                        break;
                    case 2:
                        addOpenImageIntent();


                        break;
                    case 3:
                        addCameraIntent();
                        break;
                    case 4:
                        if (UtilMetods.isOnline()){

                            if (imageOpenGlobalUri != null) {
                                AsyncClass asyncClass = new AsyncClass();
                                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
                                    asyncClass.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
                                }
                                else {
                                    asyncClass.execute((Void) null);
                                }





                            }
                            else Toast.makeText(ImagePickActivity.this,"Please, pick image at first", Toast.LENGTH_SHORT).show();
                        }
                        else Toast.makeText(ImagePickActivity.this,"Network is unreachable", Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
                        //if (UtilMetods.isOnline()) {
                            addShareIntent();
                        //}
                        //else Toast.makeText(ImagePickActivity.this,"Network is unreachable", Toast.LENGTH_SHORT).show();
                        break;
                }
                return false;
            }
        };
    }

    private Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
            return Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    private void addOpenImageIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE);
    }

    private void addCameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(RequestCode.MEDIA_TYPE_IMAGE); // create a file to save the image
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        Log.v(TAG + "makeCameraScreen", fileUri.toString());
        // start the image capture Intent
        startActivityForResult(cameraIntent, RequestCode.REQUEST_CODE_CAMERA_SCREENSHOT);
    }

    private void addShareIntent() {
        if (bitmap != null) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            startActivity(Intent.createChooser(share, "Share Image"));
        }
        else Toast.makeText(ImagePickActivity.this,"Please, pick image at first", Toast.LENGTH_SHORT).show();

    }

    private AccountHeader createAccountHeader() {
        IProfile profile = new ProfileDrawerItem()
                .withName("giffel")
                .withEmail("Sasha1220_93@mail.ru")
                .withIcon(getResources().getDrawable(R.mipmap.ic_verified_user_black_18dp));

        return new AccountHeaderBuilder()
                .withActivity(ImagePickActivity.this)
                .withHeaderBackground(R.drawable.backgroud_matherial)
                .addProfiles(profile)
                .build();
    }

    private void decodeAndViewImage(final Uri uri) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {


                BitmapFactory.Options options = new BitmapFactory.Options();

                options.inJustDecodeBounds = true;


                InputStream stream = null;

                try {
                    stream = getContentResolver().openInputStream(uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                imageUri = uri;

                bitmap = BitmapFactory.decodeStream(stream, null, options);

                try {
                    if (stream != null) {
                        stream.close();
                    } else Log.v(TAG + " ", "Stream is NULL");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                InputStream stream2 = null;

                try {
                    stream2 = getContentResolver().openInputStream(
                            uri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;
                String imageType = options.outMimeType;
                Log.v(TAG, "Image Height = " + String.valueOf(imageHeight));
                Log.v(TAG, "Image Width = " + String.valueOf(imageWidth));
                Log.v(TAG, "Image type = " + String.valueOf(imageType));

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
//                display.getSize(size); //TODO SIZE or use anouther method 2.3.7 error, requred API LVL 13
//
//                int width = size.x;
//                int height = size.y;
                int width=display.getWidth();
                int height=display.getHeight();

                options.inSampleSize = calculateInSampleSize(options, width, height);
                Log.v(TAG, "options.inSampleSize = " + String.valueOf(options.inSampleSize));
                options.inJustDecodeBounds = false;

                bitmap = BitmapFactory.decodeStream(stream2, null, options);
                try {
                    if (stream2 != null) {
                        stream2.close();
                    } else Log.v(TAG, "Stream2 is NULL");
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();


        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ImagePickActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imageView.setImageBitmap(bitmap);
            }
        });


    }

    private void saveDecodedImage() {
        File file = new File(Environment.getExternalStorageDirectory() + "/tmp.jpg");

        if (file.exists()) {
            file.delete();
        }

        try {
            if (file.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        OutputStream fOutputStream = null;
        File filetmp = new File(Environment.getExternalStorageDirectory() + "/", "tmp.jpg");

        try {
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), filetmp.getAbsolutePath(), filetmp.getName(), filetmp.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        Log.v(TAG, "Image Height calculateInSampleSize = " + String.valueOf(height));
        Log.v(TAG, "Image Width calculateInSampleSize = " + String.valueOf(width));

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public void pickImage(View View) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE);

    }

    public void dbButtonListener(View view) {
        Intent intent = new Intent(ImagePickActivity.this, SQLiteDBView.class);
        intent.putExtra("operation", "read"); // Просмотр базы при нажатии на кнопку
        startActivity(intent);

    }

    public void shareImage(View view) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_STREAM, imageUri);
        startActivity(Intent.createChooser(share, "Share Image"));
    }

    public void uploadImageButtonListener(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, RequestCode.REQUEST_CODE_UPLOAD);
    }

    public void makeCameraScreenShot(View view) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(RequestCode.MEDIA_TYPE_IMAGE); // create a file to save the image
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        Log.v(TAG + "makeCameraScreen", fileUri.toString());
        // start the image capture Intent
        startActivityForResult(intent, RequestCode.REQUEST_CODE_CAMERA_SCREENSHOT);
    }

    private static Uri getOutputMediaFileUri(int type) {
        Log.v(TAG + "MyCameraApp Uri", Uri.fromFile(getOutputMediaFile(type)).toString());
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "MyCameraApp");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG + "MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == RequestCode.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
            Log.v(TAG + "Path = ", mediaFile.getPath());
            Log.v(TAG + "Path URI = ", mediaFile.toURI().toString());
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case RequestCode.REQUEST_CODE_CAMERA_SCREENSHOT:

                    Uri uri2 = fileUri;
                    Log.v(TAG + "ActivityResultUri ", uri2.toString());

//                    if (uri2 != null) {
//                        String path = uri2.toString();
//                        if (path.toLowerCase().startsWith("file://")) {
//                            // Selected file/directory path is below
//                            path = (new File(URI.create(path))).getAbsolutePath();
//                            Log.i(TAG + "LOG", "Pick completed: " + path);
//                        }
//                    } else Log.e(TAG + "LOG", "URI2 = : NULL");
//                    try {
//                        // We need to recyle unused bitmaps
//                        if (bitmap != null) {
//                            bitmap.recycle();
//                        }
//                        InputStream stream = getContentResolver().openInputStream(uri2);
//
//
//                        bitmap = BitmapFactory.decodeStream(stream);
//                        stream.close();
//
//
//                        imageView.setImageBitmap(bitmap);
//                    } catch (FileNotFoundException e) {
//                        Log.i(TAG + "LOG", "File not Founded");
//                        e.printStackTrace();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    // TODO Make the same in refact branch
                    decodeAndViewImage(fileUri);
                    File file2 = createCameraTmpFile(); // MyCameraApp - File
                    uri3 = Uri.fromFile(file2);
                    makeSmallSizeImage(file2, uri3);
                    imageOpenGlobalUri = uri3;
//                    buttonShare.setEnabled(true);
//                    buttonUpload.setEnabled(true);
                    break;

                case RequestCode.REQUEST_CODE_OPEN_EXPLORER_IMAGE:

                    decodeAndViewImage(data.getData());
                    imageOpenGlobalUri = Uri.parse(data.getData().toString());
                    Log.v("myLog", "Add link = " + imageOpenGlobalUri);

                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.custom_toast,
                            (ViewGroup) findViewById(R.id.toast_layout_root));

                    TextView text = (TextView) layout.findViewById(R.id.text);
                    text.setText("This is a custom toast");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.setDuration(Toast.LENGTH_LONG);
                    toast.setView(layout);
                    toast.show();

//                    buttonShare.setEnabled(true);
//                    buttonUpload.setEnabled(true);
                    break;
                case RequestCode.REQUEST_CODE_UPLOAD:
                    if (data.getData() == null) Log.v("myLog", "Param data is empty");
                    decodeAndViewImage(data.getData()); // We must to upload already decoded image!!
                    saveDecodedImage(); // image saved path tmp.jpg
                    File file = new File(Environment.getExternalStorageDirectory() + "/tmp.jpg");
                    uploadImage(file.getAbsolutePath()); // download decoded image to server
                    //file.delete(); // delete temporary file
                    Uri cameraUri = getOutputMediaFileUri(RequestCode.MEDIA_TYPE_IMAGE);
                    Log.v("myLog", data.getData().getEncodedAuthority() + " " + data.getData().getEncodedPath());
                    Log.v("myLog", "Uri = " + cameraUri);
                    addToDBLink(data.getData().toString());
                    //uploadImage(data.getData());

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void makeSmallSizeImage(File file, Uri uri2) {
        decodeFile(uri2, file);
    }

    private void decodeFile(Uri uri, File file) {
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;


        InputStream stream = null;

        try {
            stream = getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap bitmapSaved = BitmapFactory.decodeStream(stream, null, options);

        try {
            if (stream != null) {
                stream.close();
            } else Log.v(TAG + " ", "Stream is NULL");
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream stream2 = null;

        try {
            stream2 = getContentResolver().openInputStream(
                    uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        String imageType = options.outMimeType;
        Log.v(TAG, "Image Height = " + String.valueOf(imageHeight));
        Log.v(TAG, "Image Width = " + String.valueOf(imageWidth));
        Log.v(TAG, "Image type = " + String.valueOf(imageType));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
//                display.getSize(size); //TODO SIZE or use anouther method 2.3.7 error, requred API LVL 13
//
//                int width = size.x;
//                int height = size.y;
        int width=display.getWidth();
        int height=display.getHeight();

        options.inSampleSize = calculateInSampleSize(options, width, height);
        Log.v(TAG, "options.inSampleSize = " + String.valueOf(options.inSampleSize));
        options.inJustDecodeBounds = false;

        bitmapSaved = BitmapFactory.decodeStream(stream2, null, options);



        OutputStream fOutputStream = null;
//        File filetmp = new File(Environment.getExternalStorageDirectory() + "/", "tmp.jpg");

        try {
            fOutputStream = new FileOutputStream(file);

            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);
            fOutputStream.flush();
            fOutputStream.close();

            MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            if (stream2 != null) {
                stream2.close();
            } else Log.v(TAG, "Stream2 is NULL");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File createCameraTmpFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "MyCameraApp");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG + "MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        Date data = new Date();
        Log.v(TAG, "" + data.getTime());
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
            Log.v(TAG + "Path = ", mediaFile.getPath());
            Log.v(TAG + "Path URI = ", mediaFile.toURI().toString());
        return mediaFile;
    }

    private void uploadImage(String absoluteFilePath) {
        final String absFilePath = absoluteFilePath;
        //HttpResponse response = null;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                String textFile = absFilePath;
                Log.v(TAG + "TAG", "textFile: " + textFile);

                // the URL where the file will be posted
                String postReceiverUrl = "http://deviantsart.com/upload.php";
                Log.v(TAG + "TAG", "postURL: " + postReceiverUrl);

                // new HttpClient
                HttpClient httpClient = new DefaultHttpClient();

                // post header
                HttpPost httpPost = new HttpPost(postReceiverUrl);

                File file = new File(textFile);
                if (file.exists()) {
                    Log.v(TAG + "TAG", "File exists");
                }
                FileBody fileBody = new FileBody(file);

                CustomMultiPartEntity entity = new CustomMultiPartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE, new CustomMultiPartEntity.ProgressListener()
                {
                    int lastPercent = 0;

                    // num - count of uploaded bytes
                    @Override
                    public void transferred(long num) {

                        percentUploaded = (int)(((float)num) / ((float)totalLength)  * 100);

                        Log.d(TAG, "percent uploaded: " + percentUploaded + " - " + num + " / " + totalLength);
                        if (lastPercent != percentUploaded)
                        {

                            lastPercent = percentUploaded;
                        }
                    }

                });

                MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                reqEntity.addPart("file", fileBody);
                httpPost.setEntity(reqEntity);

                // execute HTTP post request

                try {
                    response = httpClient.execute(httpPost);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {


                    try {
                        responseStr = EntityUtils.toString(resEntity).trim();
                        //Log.v(TAG + "TAG!!!", "Response: " + responseStr);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //String jsonObject = responseStr;

//                final ResultProcessing resultProcessing = new ResultProcessing();
//                finalResponseStr = responseStr;
//                return resultProcessing.getImageUrl(finalResponseStr);

//                final String finalResponseStr = responseStr;

//                MyActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        editTextUrlResult.setText(resultProcessing.getImageUrl(finalResponseStr));
//                    }
//                });


                    // you can add an if statement here and do other actions based on the response
                }


            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void addToDBLink(String filePath) {
        Intent intent = new Intent(ImagePickActivity.this, SQLiteDBView.class);
        intent.putExtra("operation", "add"); // Просмотр базы при нажатии на кнопку
        intent.putExtra("path", filePath);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {

        if (drawerBuilder != null && drawerBuilder.isDrawerOpen()) {
            drawerBuilder.closeDrawer();
        } else {
            ImagePickActivity.this.finish();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_about) {
//            Toast.makeText(ImagePickActivity.this, "PhotoLoader, Version 2.0", Toast.LENGTH_SHORT).show();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

//    public boolean isOnline() {
//        ConnectivityManager cm =
//                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        return netInfo != null && netInfo.isConnectedOrConnecting();
//    }

    private boolean isCameraImage() {
        if (cameraImage == true) return true;
        return false;
    }

    private class AsyncClass extends AsyncTask<Void, Integer, Void>{

        ProgressBar progressBar;
        private int progress = 10;
        //NotificationCompat.Builder notification;
//        Notification notification;
//        NotificationManager notificationManager;

        ProgressDialog pd;
        long totalSize;

        @Override
        protected void onPreExecute()
        {
            pd = new ProgressDialog(ImagePickActivity.this);
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setMessage("Uploading Picture...");
            pd.setCancelable(false);


            //notification = new NotificationCompat.Builder(getApplicationContext());




//            notification = new Notification(R.drawable.notification_template_icon_bg, "simulating a download", System
//                    .currentTimeMillis());
//            notification.flags = notification.flags | Notification.FLAG_ONGOING_EVENT;
//            notification.contentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.download_progress);
//            notification.contentView.setImageViewResource(R.id.status_icon, R.drawable.abc_ic_menu_paste_mtrl_am_alpha);
//            notification.contentView.setTextViewText(R.id.status_text, "simulation in progress");
//            notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);

//            notificationManager = (NotificationManager) getApplicationContext().getSystemService(
//                    getApplicationContext().NOTIFICATION_SERVICE);

//            notificationManager.notify(42, notification);







            pd.show();

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pd.cancel();
            return;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //notification.contentView.setProgressBar(R.id.status_progress, 100, values[0], false);
            //notificationManager.notify(42, notification);
            pd.setProgress(values[0]);
            if (values[0] >= 98){
                pd.setProgress(100);
                //notification.contentView.setProgressBar(R.id.status_progress, 100, values[0], true);
                //notificationManager.notify(42, notification);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            decodeAndViewImage(imageOpenGlobalUri); // We must to upload already decoded image!!
            saveDecodedImage();
            File file = new File(Environment.getExternalStorageDirectory() + "/tmp.jpg");




            final String absFilePath = file.getAbsolutePath();
            //HttpResponse response = null;



                    String textFile = absFilePath;
                    Log.v(TAG + "TAG", "textFile: " + textFile);

                    // the URL where the file will be posted
                    String postReceiverUrl = "http://deviantsart.com/upload.php";
                    Log.v(TAG + "TAG", "postURL: " + postReceiverUrl);

                    // new HttpClient
                    HttpClient httpClient = new DefaultHttpClient();

                    // post header
                    HttpPost httpPost = new HttpPost(postReceiverUrl);

                    file = new File(textFile);
                    if (file.exists()) {
                        Log.v(TAG + "TAG", "File exists");
                    }
                    FileBody fileBody = new FileBody(file);
                    totalLength = fileBody.getContentLength();
                    CustomMultiPartEntity reqEntity = new CustomMultiPartEntity(
                            HttpMultipartMode.BROWSER_COMPATIBLE, new CustomMultiPartEntity.ProgressListener()
                    {
                        int lastPercent = 0;

                        @Override
                        public void transferred(long num) {

                            percentUploaded = (int)(((float)num) / ((float)totalLength)  * 99f);
                            //Log.d("myLog", "percent uploaded 1: " + percentUploaded + " - " + num + " / " + totalLength);
                            try {
                                TimeUnit.MILLISECONDS.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (num <= totalLength)
                            {

                                percentUploaded = (int) ( ( (float)num * 99f ) / ( (float)totalLength) );
//                                pd.setProgress(percentUploaded);
                                Log.d("myLog", "percent uploaded: " + percentUploaded + " - " + num + " / " + totalLength);
                                progress = percentUploaded;
                                publishProgress(percentUploaded);
//                                notification.contentView.setProgressBar(R.id.status_progress, 100, progress, false);
//                                notificationManager.notify(42, notification);
                                //lastPercent = percentUploaded;
                            }
                            if (percentUploaded >= 98){
                                publishProgress(100);
                            }

                        }

                    });

                    //MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
                    reqEntity.addPart("file", fileBody);
                    httpPost.setEntity(reqEntity);

                    // execute HTTP post request

                    try {
                        response = httpClient.execute(httpPost);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    HttpEntity resEntity = response.getEntity();
                    if (resEntity != null) {


                        try {
                            responseStr = EntityUtils.toString(resEntity).trim();
                            Log.v("myLog", "Response: " + responseStr);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }






            file.delete();
            Log.v("myLog", imageOpenGlobalUri.getEncodedAuthority() + " " + imageOpenGlobalUri.getEncodedPath());
            addToDBLink(imageOpenGlobalUri.toString());
            pd.cancel();
//            notificationManager.cancel(42);
            ResultProcessing resultProcessing = new ResultProcessing();
            String finalUrl = responseStr;
            Log.v(TAG + " = ", resultProcessing.getImageUrl(finalUrl));
            // TODO FIX TO UPLOAD SMALL SIZE IMAGE, May be tmp. Origin image will download now.
            return null;
        }




    }
}



